"""Drf url filter tests"""
from rest_framework.exceptions import ParseError

from candelapp.international.models import Language
from candelapp.pagination import CustomPaginationFieldNames
from candelapp.settings.models import Settings
from candelapp.tools import test_utils
from candelapp.website import factories


class URLFilterTestCase(test_utils.TestQueryParamsMixin, test_utils.APITestCaseWithData):
    """Test cases using filtering."""

    @classmethod
    def setUpTestData(cls):
        """Set up objects and users."""
        super().setUpTestData()
        settings = Settings.objects.first()
        cls.primary_lang = settings.primary_language
        cls.cv_lang = Language.objects.get(code="cv")
        settings.secondary_languages.add(cls.cv_lang)

        cls.model1 = factories.CModelFactory(type="multiple")
        cls.model2 = factories.CModelFactory(type="multiple")
        cls.model2.fields.append(
            {
                "internal_name": "some_relation",
                "verbose_name": "Some relation",
                "required": False,
                "type": "manytoone",
                "model": cls.model1.pk,
                "extras": [],
            }
        )
        cls.model2.save(update_fields=["fields"])
        cls.modelitem1 = factories.CModelItemFactory(
            model=cls.model1,
            data={
                "title": "Hey1",
                "price": 1,
                "written": "azerty",
                "animals": ["cats", "dogs"],
                "fruits": "apple",
            },
        )

        cls.modelitem2 = factories.CModelItemFactory(
            model=cls.model1,
            data={
                "title": "Hey2",
                "price": 2,
                "written": "bepo",
                "animals": ["ferrets"],
                "fruits": "apple",
            },
        )
        cls.modelitem3 = factories.CModelItemFactory(
            model=cls.model1,
            data={
                "title": "Hey3",
                "price": 3,
                "written": "qwerty",
                "animals": ["dogs", "ferrets"],
                "fruits": "apple",
            },
        )

        cls.modelitem4 = factories.CModelItemFactory(
            model=cls.model2,
            data={
                "title": "Hey4",
                "price": 4.2,
                "written": "bye",
                "animals": ["dogs"],
                "fruits": "orange",
            },
        )
        cls.modelitem5 = factories.CModelItemFactory(
            model=cls.model2,
            data={
                "title": "Hey5",
                "price": 5.4,
                "written": "bye",
                "animals": ["cats"],
                "fruits": "orange",
            },
        )
        cls.modelitem5 = factories.CModelItemFactory(
            model=cls.model2,
            data={
                "title": "Hey5",
                "price": 5.4,
                "written": "bye",
                "animals": ["cats"],
                "fruits": "orange",
                "some_relation": [cls.modelitem1.pk, cls.modelitem2.pk],
            },
        )
        cls.modelitem6 = factories.CModelItemFactory(
            model=cls.model2,
            data={
                "title": "Hey5",
                "price": 5.4,
                "written": "bye",
                "animals": ["cats"],
                "fruits": "orange",
                "some_relation": [cls.modelitem1.pk],
            },
        )

    def test_not_available_relations(self):
        # With modelitem queryset and a relation
        # It should raise an error
        with self.assertRaisesMessage(ParseError, "Filtering relations is not yet available."):
            self._test_qp("field__relation_field__contains=Hey", self.model1)

    def test_field_unknown(self):
        # With an unknown field
        # It should raise an error that the specified field is not a valid one
        with self.assertRaisesMessage(ParseError, "super_field is not a valid field to filter."):
            self._test_qp("super_field__contains=hey", self.model1)

    def test_exact_number_field_with_range(self):
        # When filtering a number with the exact lookup and a range
        # It should raise an error
        with self.assertRaisesMessage(
            ParseError, "price with exact lookup expected a number. Got a string."
        ):
            self._test_qp("price=1-2", self.model1)

    def test_wrong_range(self):
        # When filtering with a bad range formatting
        # It should raise an error
        with self.assertRaisesMessage(
            ParseError, "price with exact lookup expected a number. Got a string."
        ):
            self._test_qp("price=1-2-3", self.model1)

    def test_wrong_lookups(self):
        # With an unknown lookup
        # It should raise an error that the specified field is not a valid one
        with self.assertRaisesMessage(
            ParseError, "bad_lookup is not a valid lookup for the field title."
        ):
            self._test_qp("title__bad_lookup=hey", self.model1)

        # With a known lookup but not available to the type "text"
        # It should raise an error that the specified field is not a valid one
        with self.assertRaisesMessage(ParseError, "gte is not a valid lookup for the field title."):
            self._test_qp("title__gte=hey", self.model1)

    def test_filter_none(self):
        # When filtering the api with a non existing title
        qs = self._test_qp("title=Unknown", self.model1)
        # We should get a none queryset
        assert qs.count() == 0

    def test_filter_strings(self):
        # When filtering the api with one exact title
        qs = self._test_qp("title=Hey1", self.model1)
        # We should get a queryset with the object titled Hey1
        assert qs.count() == 1
        assert qs[0].data["title"] == "Hey1"

        # When filtering two titles will use OR operand and filter both
        qs = self._test_qp("title=Hey1,Hey2", self.model1)
        # We should get a qs with the two objects titled Hey1 & Hey2
        assert qs.count() == 2
        assert qs[0].data["title"] in ["Hey1", "Hey2"]
        assert qs[1].data["title"] in ["Hey1", "Hey2"]

        # When filtering with icontains
        qs = self._test_qp("title__icontains=hey", self.model1)
        # We should got none as it is sensitive cased
        qs.count() == 0

        # When filtering with contains
        qs = self._test_qp("title__contains=hey", self.model1)
        # We should get all of them as they all start with hey
        qs.count() == 2

        # When filtering with the written field with endswith
        qs = self._test_qp("written__endswith=zerty", self.model1)
        # We should get 2 objects
        qs.count() == 1

    def test_filter_choices(self):
        # When filtering the api with objects containing orange in  choice
        qs = self._test_qp("fruits__contains=orange", self.model1)

        # We should get all the objects with dogs
        qs.count() == 2
        for obj in qs:
            assert "orange" == obj.data["fruits"]

    def test_filter_multiplechoices(self):
        # When filtering the api with objects containing dogs in multiple choice
        qs = self._test_qp("animals__contains=dogs", self.model1)

        # We should get all the objects with dogs
        qs.count() == 3
        for obj in qs:
            assert "dogs" in obj.data["animals"]

        # Filtering the api with objects containing dogs and ferrets in multiple choice
        qs = self._test_qp("animals__contains=dogs,ferrets", self.model1)

        # We should get all the objects with dogs and ferrets
        qs.count() == 4

    def test_filter_numbers(self):
        # When filtering the api to get objects with an exact price of 3
        qs = self._test_qp("price=3", self.model1)

        # We should get 1
        assert qs.count() == 1

        # When filtering the api to get objects with price gte 4
        qs = self._test_qp("price__gte=4.2", self.model2)

        # We should get 4
        assert qs.count() == 4

        # When filtering with range between 1 and 3
        qs = self._test_qp("price__range=1-3", self.model1)
        # Should get 3 (1, 2, 3)
        assert qs.count() == 3

    def test_filter_manytoones(self):
        # When filtering on a manytoone
        qs = self._test_qp("some_relation=1", self.model2)
        # We should get one item
        assert qs.count() == 2

        # When filtering on a fk with OR values
        qs = self._test_qp("some_relation=1,2", self.model2)
        # # We should get two objects
        assert qs.count() == 2

        # # When filtering on a fk with multiple and values
        qs = self._test_qp("some_relation=1&some_relation=4", self.model2)
        # # We should get none as a modelitem with fk 4 doesnt exist
        assert qs.count() == 0

        # # When filtering on a fk with multiple and values
        qs = self._test_qp("some_relation=1,4", self.model2)
        # # We should get one object as 1 exist but not 4
        assert qs.count() == 2

    def test_complex_filtering(self):
        # When filtering with title exact and price lte too low
        qs = self._test_qp("title=Hey1&price__lte=0.5", self.model1)
        # We should get a none queryset
        assert qs.count() == 0

        # When filtering with title exact and price lte
        qs = self._test_qp("title=Hey1&price__lte=1", self.model1)
        # We should get a queryset with the object named Hey1
        assert qs.count() == 1

        # When filtering a range of price with written containing letters erty
        qs = self._test_qp("written__icontains=erty&price__range=1-5", self.model1)
        # We should get two objects with qwerty and azerty and the price in between
        assert qs.count() == 2

        # When filtering a range of price between 2 and 3.5, a title containing hey
        # with animals such as dogs and ferrets and an apple fruit
        qs = self._test_qp(
            "title__icontains=hey&price__range=2-3.5"
            "&animals__contains=ferrets&animals__contains=dogs"
            "&fruits=apple",
            self.model1,
        )
        # We should get one queryset with one object
        assert qs.count() == 1

    def test_filter_protected_fields(self):
        """Protected pagination fields are ignored"""
        limit = CustomPaginationFieldNames.limit_query_param
        offset = CustomPaginationFieldNames.offset_query_param
        q = f"{limit}=1&{offset}=1"
        qs = self._test_qp(q, self.model1)
        assert qs.count() == 3
