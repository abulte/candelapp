from django.apps import AppConfig


class DRFUrlFiltersConfig(AppConfig):
    name = "candelapp.drf_url_filters"
