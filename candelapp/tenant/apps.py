from django.apps import AppConfig


class TenantConfig(AppConfig):
    name = "candelapp.tenant"
