from candelapp.tenant.models import Domain, Tenant
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django_tenants.utils import tenant_context

tenants = [
    {"schema_name": "public", "domains": ["localhost"]},
    {"schema_name": "sacredheart", "domains": ["api.sacredheart.localhost"]},
    {"schema_name": "fsociety", "domains": ["api.fsociety.localhost"]},
]


USER_MODEL = get_user_model()


class Command(BaseCommand):
    help = "Setup dev tenants"

    def handle(self, *args, **options):
        """Check existence of public and customer schema or create them."""
        if settings.CONF != "Dev":
            raise Exception("This is for development use only !")
        qs = Tenant.objects.filter(schema_name__in=["sacredheart"])
        self.schemas = [el.schema_name for el in qs]
        if len(self.schemas) == 0:
            self._create_tenants()
            self._create_users()
            self.stdout.write(self.style.SUCCESS("Public and customer tenants are ready."))
        else:
            self.stdout.write(self.style.SUCCESS("Tenants already are setup"))

    def _create_tenants(self):
        """Create customer tenant."""
        for item in tenants:
            if item["schema_name"] in self.schemas:
                pass
            else:
                tenant = Tenant(schema_name=item["schema_name"], name=item["schema_name"])
                tenant.save()
                for domain in item["domains"]:
                    domain = Domain(domain=domain, tenant=tenant, is_primary=True)
                    domain.save()

    def _create_users(self):
        """Create an admin user for the customer schema."""
        for tenant in tenants:
            schema_name = tenant["schema_name"]
            if schema_name in self.schemas or schema_name == "public":
                continue
            tenant = Tenant.objects.get(schema_name=schema_name)
            with tenant_context(tenant):
                user1 = USER_MODEL()
                user1.email = "user@candelapp.com"
                user1.email_confirmed = True
                user1.first_name = "John"
                user1.last_name = "Dorian"
                user1.is_staff = False
                user1.is_superuser = False
                user1.set_password("candelapp")
                user1.save()

                user2 = USER_MODEL()
                user2.email = "admin@candelapp.com"
                user2.email_confirmed = True
                user2.first_name = "Bob"
                user2.last_name = "Kelso"
                user2.is_staff = True
                user2.is_superuser = True
                user2.set_password("candelapp")
                user2.save()
