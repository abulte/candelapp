from django.core.management.base import BaseCommand

from django.contrib.auth import get_user_model

from django_tenants.utils import tenant_context

from candelapp.settings.models import Settings
from candelapp.tenant.models import Domain, Tenant
from candelapp.tools import tools


User = get_user_model()


class Command(BaseCommand):
    help = "Setup new tenant"

    def add_arguments(self, parser):
        parser.add_argument("schema_name", type=str)
        parser.add_argument("domain", type=str)
        parser.add_argument("admin_email", type=str)
        parser.add_argument("admin_firstname", type=str)
        parser.add_argument("admin_lastname", type=str)

    def handle(self, *args, **options):
        """Check existence of public and customer schema or create them."""
        self.create_tenant(**options)
        self.tenant = Tenant.objects.get(schema_name=options["schema_name"])

        self.create_user(**options)

        self.create_settings()

        self.stdout.write(
            self.style.SUCCESS("The new tenant {} is now setup.".format(self.tenant.name))
        )

    def create_tenant(self, **options):
        """Create tenant."""
        tenant = Tenant(schema_name=options["schema_name"], name=options["schema_name"])
        tenant.save()
        domain = Domain(domain=options["domain"], tenant=tenant, is_primary=True)
        domain.save()

    def create_user(self, **options):
        """Create an admin user for the customer schema."""
        with tenant_context(self.tenant):
            user = User()
            user.username = "user"
            user.email = options["admin_email"]
            user.is_staff = True
            user.first_name = options["admin_firstname"]
            user.last_name = options["admin_lastname"]

            password = tools.generate_password()
            user.set_password(password)
            user.save()

            self.stdout.write(
                self.style.SUCCESS("New user created.\nEmail: {}\nPassword: {}").format(
                    user.email, password
                )
            )

    def create_settings(self):
        """Create empty settings for the tenant."""
        with tenant_context(self.tenant):
            Settings.objects.create()

        self.stdout.write(self.style.SUCCESS("Settings created."))
