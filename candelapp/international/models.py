"""International models."""
from django.db import models
from django.utils.translation import gettext_lazy as _

from candelapp.international.languages import LANGUAGES


LANGUAGES_DICT = dict(LANGUAGES)


class Language(models.Model):
    """Lang model."""

    code = models.CharField(max_length=2)
    secondary_language = models.ForeignKey(
        "settings.Settings",
        null=True,
        related_name="secondary_languages",
        on_delete=models.SET_NULL,
    )

    class Meta:
        verbose_name = _("Language")
        verbose_name_plural = "Languages"

    def get_translated_name(self):
        return LANGUAGES_DICT.get(self.code)

    def __str__(self):
        return str(self.get_translated_name())
