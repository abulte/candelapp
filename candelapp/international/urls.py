from django.urls import path

from rest_framework import routers

from candelapp.international import views


router = routers.SimpleRouter()

urlpatterns = [
    path(r"languages/", views.LanguageAPIListView.as_view()),
]

urlpatterns += router.urls
