"""International views."""
from rest_framework import generics
from rest_framework.permissions import IsAdminUser

from candelapp.international import models
from candelapp.international import serializers


class LanguageAPIListView(generics.ListAPIView):
    """View to list languages."""

    permission_classes = [IsAdminUser]
    serializer_class = serializers.LanguageSerializer
    queryset = models.Language.objects.all()
