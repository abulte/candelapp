from django.db import models

from candelapp.website.models import tenant_images_folder


def get_default_lang():
    """
    Get english as default language for the generated website.

    As far the language list doesn't get updated, english language
    id is 38.
    """
    return 38


class Settings(models.Model):
    """Settings model that stores customer specific data."""

    ftp_host = models.CharField(max_length=250, blank=True)
    ftp_user = models.CharField(max_length=250, blank=True)
    ftp_password = models.CharField(max_length=250, blank=True)
    ftp_port = models.IntegerField(default=21)

    site_name = models.CharField(max_length=100)
    site_url = models.URLField(max_length=200)

    primary_language = models.ForeignKey(
        "international.Language", default=get_default_lang, on_delete=models.PROTECT
    )

    logo = models.ImageField(upload_to=tenant_images_folder, blank=True)
    colors = models.JSONField(default=dict)

    def save(self, *args, **kwargs):
        """Restrain to only one settings."""
        if self.__class__.objects.count():
            self.pk = self.__class__.objects.first().pk
        super().save(*args, **kwargs)
