"""Settings views."""
from rest_framework import status, views
from rest_framework.permissions import IsAdminUser, IsAuthenticatedOrReadOnly
from rest_framework.response import Response

from candelapp.settings import models
from candelapp.settings import serializers


class SettingsAPIView(views.APIView):
    """View to get and update settings."""

    permission_classes = [IsAdminUser]
    serializer_class = serializers.SettingsSerializer

    def get(self, request):
        """Get method for settings."""
        settings, _ = models.Settings.objects.get_or_create()
        serializer = self.serializer_class(settings)
        return Response(serializer.data)

    def patch(self, request, format=None):
        """Patch method for settings."""
        settings, _ = models.Settings.objects.get_or_create()
        serializer = self.serializer_class(settings, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PasswordFTPView(views.APIView):
    permission_classes = [IsAdminUser]
    serializer_class = serializers.PasswordFTPSerializer

    def patch(self, request, format=None):
        """Patch method for settings."""
        settings, _ = models.Settings.objects.get_or_create()
        serializer = self.serializer_class(settings, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.errors, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class InterfaceSettingsAPIView(views.APIView):
    """View to get interface settings only from anyone."""

    permission_classes = [IsAuthenticatedOrReadOnly]
    serializer_class = serializers.InterfaceSettingsSerializer

    def get(self, request):
        settings, _ = models.Settings.objects.get_or_create()
        serializer = self.serializer_class(settings)
        return Response(serializer.data)


class LanguageSettingsAPIView(views.APIView):
    """View to get languages set in settings."""

    permission_classes = [IsAdminUser]
    serializer_class = serializers.LanguageSettingsSerializer

    def get(self, request):
        """Get method for language settings."""
        settings, _ = models.Settings.objects.get_or_create()
        serializer = self.serializer_class(settings)
        return Response(serializer.data)
