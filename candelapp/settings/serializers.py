"""Tenant related seiralizers."""
from rest_framework import serializers

from candelapp.settings import models
from candelapp.tools.encryption import PasswordManager


error_list = {
    "primary_language_already_used": (
        "La langue principale ne peut pas être utilisée comme langue secondaire."
    ),
}


class SettingsSerializer(serializers.ModelSerializer):
    """Serializer for Settings."""

    logo = serializers.CharField(required=False, allow_blank=True)

    class Meta:
        model = models.Settings
        fields = [
            "ftp_host",
            "ftp_user",
            "ftp_port",
            "logo",
            "colors",
            "primary_language",
            "secondary_languages",
            "site_name",
            "site_url",
        ]

    def validate(self, data):
        """Ensure that primary language is not in secondary ones and vice versa."""

        def raise_error():
            raise serializers.ValidationError(error_list["primary_language_already_used"])

        primary_language = data.get("primary_language")
        secondary_languages = data.get("secondary_languages")

        if primary_language and secondary_languages:
            if primary_language in secondary_languages:
                raise_error()
        elif primary_language and not secondary_languages:
            secondary_languages_pks = self.instance.secondary_languages.all()
            if primary_language in secondary_languages_pks:
                raise_error()
        elif secondary_languages and not primary_language:
            if self.instance.primary_language in secondary_languages:
                raise_error()

        return data


class PasswordFTPSerializer(serializers.ModelSerializer):
    """Update password serializer."""

    ftp_password = serializers.CharField(min_length=8, write_only=True)

    class Meta:
        model = models.Settings
        fields = [
            "ftp_password",
        ]

    def update(self, instance, validated_data):
        """Sign password so it's not plain stored in the database."""
        ftp_password = validated_data.get("ftp_password")
        pm = PasswordManager()
        validated_data["ftp_password"] = pm.encrypt(ftp_password)
        return super().update(instance, validated_data)


class InterfaceSettingsSerializer(serializers.ModelSerializer):
    """Read only interface settings serializer."""

    logo = serializers.CharField()

    class Meta:
        model = models.Settings
        fields = [
            "colors",
            "logo",
        ]
        read_only_fields = [
            "colors",
            "logo",
        ]


class LanguageSettingsSerializer(serializers.ModelSerializer):
    """Serializer for LanguageSettings view."""

    class Meta:
        model = models.Settings
        fields = [
            "primary_language",
            "secondary_languages",
        ]

    def to_representation(self, instance):
        """Converts languages to a better representation."""
        ret = super().to_representation(instance)
        primary_language = getattr(instance, "primary_language")
        if primary_language:
            ret["primary_language"] = {
                "pk": instance.primary_language.pk,
                "code": instance.primary_language.code,
                "name": instance.primary_language.get_translated_name(),
            }
        secondary_languages = getattr(instance, "secondary_languages")
        if secondary_languages:
            ret["secondary_languages"] = []
            for language in secondary_languages.order_by("code"):
                ret["secondary_languages"].append(
                    {
                        "pk": language.pk,
                        "code": language.code,
                        "name": language.get_translated_name(),
                    }
                )

        return ret
