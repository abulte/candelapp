from django.urls import path

from rest_framework import routers

from candelapp.settings import views


router = routers.SimpleRouter()

urlpatterns = [
    path(r"interface/", views.InterfaceSettingsAPIView.as_view()),
    path(r"languages/", views.LanguageSettingsAPIView.as_view()),
    path(r"settings/", views.SettingsAPIView.as_view()),
    path(r"update_ftp_password/", views.PasswordFTPView.as_view()),
]

urlpatterns += router.urls
