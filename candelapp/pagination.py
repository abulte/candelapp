from rest_framework.pagination import LimitOffsetPagination


class CustomPaginationFieldNames(LimitOffsetPagination):
    """Avoid model field collision by using technical names for pagination args"""
    limit_query_param = "__limit"
    offset_query_param = "__offset"
