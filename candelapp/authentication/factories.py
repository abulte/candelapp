"""Authentication related factories."""

import factory

from django.contrib.auth import get_user_model


User = get_user_model()


class UserFactory(factory.django.DjangoModelFactory):
    """Factory for User."""

    class Meta:
        model = User

    email = factory.Sequence(lambda n: "user{}@candelapp.com".format(n))
    password = factory.PostGenerationMethodCall("set_password", "c4nd314pp")
    first_name = factory.Sequence(lambda n: "John {}".format(n))
    last_name = "Doe"
    is_active = True
    is_staff = False
    is_superuser = False
    email_confirmed = True


class AdminFactory(UserFactory):
    """Factory for admin User."""

    is_staff = True
    is_superuser = True
