"""Auth views."""
from copy import deepcopy

from django.contrib.auth import get_user_model

from drf_spectacular.utils import extend_schema, OpenApiParameter
from drf_spectacular.types import OpenApiTypes
from rest_framework import generics, viewsets
from rest_framework.authtoken.models import Token
from rest_framework.decorators import action
from rest_framework.filters import OrderingFilter
from rest_framework.permissions import AllowAny, IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED, HTTP_204_NO_CONTENT
from rest_framework.views import APIView

from candelapp.authentication import serializers
from candelapp.authentication.mails import send_reset_password_email
from candelapp.history.manager import LogManager

User = get_user_model()
lm = LogManager()


class UserViewset(viewsets.ModelViewSet):
    """
    A viewset for viewing and editing user instances.
    """

    serializer_class = serializers.UserSerializer
    queryset = User.objects.all()
    permission_classes = [IsAdminUser]
    http_method_names = ["get", "post", "patch", "delete"]
    filter_backends = [OrderingFilter]

    @action(detail=True, methods=["get"])
    def toggle_admin(self, request, pk=None):
        """Give admin rights to a user or remove them."""
        user = self.get_object()
        prev_state = deepcopy(user.__dict__)
        user.toggle_admin()
        lm.add_log_entry(
            type="user_toggle_admin",
            instance=user,
            user=request.user,
            prev_state=prev_state,
            new_state=user.__dict__,
        )
        return Response(status=HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        lm.add_log_entry(
            type="user_delete", user=request.user, instance=instance, prev_state=instance.__dict__
        )
        return Response(status=HTTP_204_NO_CONTENT)

    @extend_schema(
        parameters=[
            OpenApiParameter("is_admin", OpenApiTypes.BOOL, OpenApiParameter.QUERY),
            OpenApiParameter("is_confirmed", OpenApiTypes.BOOL, OpenApiParameter.QUERY),
            OpenApiParameter("with_groups", OpenApiTypes.BOOL, OpenApiParameter.QUERY),
        ],
    )
    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        # TODO: Make a generic filter
        is_admin = self.request.query_params.get("is_admin")
        if is_admin is not None:
            attr = bool(is_admin.lower() == "true")
            queryset = queryset.filter(is_staff=attr, is_superuser=attr)

        with_groups = self.request.query_params.get("with_groups")
        if with_groups is not None:
            attr = bool(with_groups.lower() == "false")
            queryset = queryset.filter(groups__isnull=attr)

        is_confirmed = self.request.query_params.get("is_confirmed")
        if is_confirmed is not None:
            attr = bool(is_confirmed.lower() == "true")
            queryset = queryset.filter(email_confirmed=attr)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        """Override create method to add logging."""
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        lm.add_log_entry(
            type="user_add", instance=instance, user=request.user, new_state=instance.__dict__
        )
        return Response(serializer.data, status=HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        instance = serializer.save()
        return instance

    def update(self, request, *args, **kwargs):
        """Override update method to add logging."""
        partial = kwargs.pop("partial", False)
        instance = self.get_object()
        prev_state = deepcopy(instance.__dict__)
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        new_state = deepcopy(instance.__dict__)
        lm.add_log_entry(
            type="user_change",
            instance=instance,
            user=request.user,
            prev_state=prev_state,
            new_state=new_state,
        )
        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)


class UserMeView(generics.RetrieveUpdateAPIView):
    """A view to get or update the user's own infos."""

    model = User
    serializer_class = serializers.UserMeSerializer
    permission_classes = [IsAuthenticated]
    http_method_names = ["get", "patch"]

    def get_object(self, *args, **kwargs):
        """Return user."""
        return self.request.user

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", False)
        instance = self.get_object()
        prev_state = deepcopy(instance.__dict__)
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        new_state = deepcopy(instance.__dict__)
        lm.add_log_entry(
            type="user_change",
            instance=instance,
            user=request.user,
            prev_state=prev_state,
            new_state=new_state,
        )
        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        return Response(serializer.data)


class SignupView(APIView):
    """A view to signup."""

    model = User
    serializer_class = serializers.UserMeSerializer
    permission_classes = [AllowAny]

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = serializer.save()
        lm.add_log_entry(type="user_add", instance=instance, new_state=instance.__dict__)
        return Response(status=HTTP_204_NO_CONTENT)


class ObtainAuthTokenView(APIView):
    """A view to get an an authentication token."""

    permission_classes = [AllowAny]
    serializer_class = serializers.AuthTokenSerializer

    def get_serializer_context(self):
        return {
            "request": self.request,
            "format": self.format_kwarg,
            "view": self,
        }

    def get_serializer(self, *args, **kwargs):
        kwargs["context"] = self.get_serializer_context()
        return self.serializer_class(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["user"]
        try:
            token = Token.objects.get(user=user)
            token.delete()
        except Token.DoesNotExist:
            pass
        token = Token.objects.create(user=user)
        lm.add_log_entry(type="user_connect", instance=user, user=user, request=request)

        return Response({"token": token.key})


class PasswordSetTokenView(APIView):
    """A view to validate an email and set password for a user."""

    permission_classes = [AllowAny]
    serializer_class = serializers.EmailValidationSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["user"]
        user.email_confirmed = True
        user.set_password(serializer.data["new_password"])
        user.save(update_fields=["email_confirmed", "password"])
        return Response(status=HTTP_204_NO_CONTENT)


class PasswordChangeView(APIView):
    """A view to change password."""

    permission_classes = [IsAuthenticated]
    serializer_class = serializers.PasswordChangeSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = request.user
        user.set_password(serializer.data["new_password"])
        user.save(update_fields=["password"])
        return Response(status=HTTP_204_NO_CONTENT)


class PasswordResetTokenView(APIView):
    """A view to send a reset token to a user."""

    permission_classes = [AllowAny]
    serializer_class = serializers.EmailSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = None
        try:
            user = User.objects.get(email=serializer.data["email"])
        except User.DoesNotExist:
            pass
        if user:
            send_reset_password_email(user)
        return Response(status=HTTP_204_NO_CONTENT)
