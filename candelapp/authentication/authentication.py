"""Custom rest framework authentication."""
from datetime import datetime, timezone

from django.conf import settings
from django.utils.translation import gettext_lazy as _

from rest_framework import authentication, exceptions


class TokenAuthenticationWithExpiration(authentication.TokenAuthentication):
    """Subclass TokenAuthentication to add token expiration.

    Use 'Bearer' as keyword name to match open api specifications.

    DRF uses the keyword 'token' by default and makes spectacular not working.

    DRF Token Authentication:
    Authorization: Token xxxxxx-xxxxxx-xxxxxxxxxx

    Bearer Authentication:
    Authorization: Bearer xxxxxx-xxxxxx-xxxxxxxxxx
    """

    keyword = "Bearer"

    def authenticate_credentials(self, key):
        # Method returns a user and a token like this "return (token.user, token)"
        user_and_token = super().authenticate_credentials(key)
        token = user_and_token[1]

        # Check if token is expired
        nb_seconds_alive_token = (datetime.now(timezone.utc) - token.created).total_seconds()
        if nb_seconds_alive_token > settings.API_TOKEN_MAX_AGE:
            token.delete()
            raise exceptions.AuthenticationFailed(_("Token expired."))

        return user_and_token
