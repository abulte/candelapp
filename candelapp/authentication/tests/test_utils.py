import base64
from datetime import datetime, timedelta

from django.conf import settings

from django.contrib.auth import get_user_model

from freezegun import freeze_time


from candelapp.authentication.factories import AdminFactory, UserFactory
from candelapp.authentication.utils import TokenManager
from candelapp.tools import test_utils


User = get_user_model()


class TokenManagerTests(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        cls.admin_user = AdminFactory(first_name="Bob", last_name="Kelso", email_confirmed=False)
        cls.non_admin_user = UserFactory(
            first_name="John", last_name="Dorian", email_confirmed=False
        )
        cls.tm = TokenManager()

    def test_timestamped_token(self):
        # With a token tied to users
        for user in [self.admin_user, self.non_admin_user]:
            token = TokenManager.generate_token(user)

            # It should be valid as it is checked right away
            assert self.tm.check_token(token) is True
            # But when we advance the time to the expiration of a token
            time = datetime.now() + timedelta(seconds=settings.PASSWORD_RESET_TIMEOUT + 1)
            with freeze_time(time):
                # It should not be valid anymore
                assert self.tm.check_token(token) is False

    def test_user(self):
        # When creating a token for a user
        user = UserFactory(email_confirmed=False)
        token = TokenManager.generate_token(user)

        # It should be valid
        assert self.tm.check_token(token) is True

        # When the user does not exist anymore
        user.delete()

        # It should not be valid anymore
        assert self.tm.check_token(token) is False

    def test_wrong_token(self):
        # When checking a non b64 token
        token = "string"
        assert self.tm.check_token(token) is False

        # When checking a token with no email attached on it
        token = base64.b64encode(b"hello")
        assert self.tm.check_token(token.decode()) is False

        # Token with non existing user
        token = base64.b64encode(b"hello:john@candelapp.com")
        assert self.tm.check_token(token.decode()) is False

        token = base64.b64encode(f"hello:{self.non_admin_user}".encode())
        assert self.tm.check_token(token.decode()) is False

    def test_get_user_from_token(self):
        # When creating a token from users
        token = TokenManager.generate_token(self.non_admin_user)
        with self.assertRaisesMessage(
            RuntimeError, "You have to call the check_token method first in order to get the user."
        ):
            self.tm.get_user_from_token

        self.tm.check_token(token)
        assert self.tm.get_user_from_token == self.non_admin_user
