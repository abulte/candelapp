"""Authentication backend."""
from typing import Optional

from django.core.exceptions import ValidationError
from django.core.validators import validate_email

from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.contrib.auth.backends import ModelBackend


UserModel = get_user_model()


class EmailModelBackend(ModelBackend):
    """Custom authentication backend that allows authentication with emails."""

    def authenticate(
        self,
        request,
        email: Optional[str] = None,
        password: Optional[str] = None,
    ) -> Optional[User]:
        """Custom Authenticate method."""

        try:
            validate_email(email)
        except ValidationError:
            return None

        try:
            user = UserModel.objects.get(email=email)
        except UserModel.DoesNotExist:
            return None
        if all(
            [
                user is not None,
                user.check_password(password),
                user.email_confirmed is True,
                self.user_can_authenticate(user),
            ]
        ):
            return user
        return None
