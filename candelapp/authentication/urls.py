from django.urls import path

from rest_framework import routers

from candelapp.authentication import views


router = routers.SimpleRouter()
router.register(r"users", views.UserViewset)

urlpatterns = router.urls

urlpatterns += [
    path("me/", views.UserMeView.as_view(), name="me_view"),
    path("login/", views.ObtainAuthTokenView.as_view(), name="login"),
    path("signup/", views.SignupView.as_view(), name="signup"),
    path("password_change/", views.PasswordChangeView.as_view(), name="password-change"),
    path("password_set_token/", views.PasswordSetTokenView.as_view(), name="password-set-token"),
    path(
        "password_reset_token/", views.PasswordResetTokenView.as_view(), name="password-reset-token"
    ),
]
