from django.core import exceptions as django_exceptions

from django.utils.translation import gettext as _

from django.contrib.auth import get_user_model
from django.contrib.auth import authenticate
from django.contrib.auth.models import Group
from django.contrib.auth.password_validation import validate_password

from rest_framework import serializers


from candelapp.authentication.utils import TokenManager
from candelapp.groups.serializers import UserGroupsSerializer


User = get_user_model()


class EmailSerializer(serializers.Serializer):
    email = serializers.EmailField()


class AuthTokenSerializer(serializers.Serializer):
    email = serializers.EmailField(label=_("Email"), write_only=True)
    password = serializers.CharField(
        label=_("Password"),
        style={"input_type": "password"},
        trim_whitespace=False,
        write_only=True,
    )
    token = serializers.CharField(label=_("Token"), read_only=True)

    def validate(self, attrs):
        email = attrs.get("email")
        password = attrs.get("password")

        if email and password:
            user = authenticate(
                request=self.context.get("request"),
                email=email,
                password=password,
            )

            # The authenticate call simply returns None for is_active=False
            # users. (Assuming the default ModelBackend authentication
            # backend.)
            if not user:
                msg = _("Unable to log in with provided credentials.")
                raise serializers.ValidationError(msg, code="authorization")
        else:
            msg = _('Must include "email" and "password".')
            raise serializers.ValidationError(msg, code="authorization")

        attrs["user"] = user
        return attrs


class UserMeSerializer(serializers.ModelSerializer):
    """User serializer for self."""

    groups = UserGroupsSerializer(many=True, read_only=True)

    class Meta:
        model = User
        fields = [
            "pk",
            "first_name",
            "last_name",
            "email",
            "extras",
            "groups",
            "is_admin",
            "is_confirmed",
        ]
        read_only_fields = [
            "groups",
            "is_admin",
            "is_confirmed",
        ]


class UserSerializer(serializers.ModelSerializer):
    """User serializer."""

    groups = serializers.PrimaryKeyRelatedField(
        queryset=Group.objects.all(), many=True, required=False
    )

    class Meta:
        model = User
        fields = [
            "pk",
            "first_name",
            "last_name",
            "email",
            "groups",
            "extras",
            "is_admin",
            "is_confirmed",
        ]
        read_only_fields = [
            "is_admin",
            "is_confirmed",
        ]

    def validate_email(self, email):
        """Ensure email is not already used."""
        if User.objects.filter(email=email).exists():
            raise serializers.ValidationError(_("This email is not available."))
        return email


class PasswordChangeSerializer(serializers.Serializer):
    """Password change serializer."""

    new_password = serializers.CharField(required=True)
    re_new_password = serializers.CharField(required=True)

    default_error_messages = {
        "password_mismatch": _("Password mismatch."),
    }

    def validate(self, attrs):
        """Validate retype on new password."""
        attrs = super().validate(attrs)
        if not attrs["new_password"] == attrs["re_new_password"]:
            return self.fail("password_mismatch")
        else:
            return attrs

    def validate_new_password(self, value: str) -> str:
        """Check strength of new password."""
        try:
            validate_password(value)
        except django_exceptions.ValidationError as e:
            raise serializers.ValidationError(list(e.messages))
        return value


class EmailValidationSerializer(PasswordChangeSerializer):
    token = serializers.CharField(required=True)

    default_error_messages = {
        "has_password": _("You already set a password."),
        "password_mismatch": _("Password mismatch."),
        "token_not_valid": _("The token is not valid or has expired."),
    }

    def validate(self, data):
        data = super().validate(data)
        tm = TokenManager()
        if tm.check_token(data["token"]) is False:
            raise serializers.ValidationError(
                {"token": self.default_error_messages["token_not_valid"]}
            )
        data["user"] = tm.get_user_from_token
        return data
