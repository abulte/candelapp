"""Authentication models."""

from django.db import models
from django.utils.translation import gettext as _

from django.contrib.auth.models import AbstractUser, UserManager


class CustomUserManager(UserManager):
    """Manager for custom User."""

    def _create_user(self, email="", password=None, **extra_fields):
        """Create and save an user."""
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, **extra_fields):
        """Create user."""
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(**extra_fields)

    def create_superuser(self, **extra_fields):
        """Create superuser."""
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        return self._create_user(**extra_fields)


class User(AbstractUser):
    """Custom user model using email as username."""

    # Drop the django default username
    username = None

    first_name = models.CharField(max_length=30, blank=False)
    last_name = models.CharField(max_length=150, blank=False)
    email = models.EmailField(_("email address"), unique=True)
    email_confirmed = models.BooleanField(
        _("email confirmed"),
        default=False,
    )
    extras = models.JSONField(_("extras field"), default=dict)

    objects = CustomUserManager()

    class Meta:
        db_table = "auth_user"

    @property
    def username(self):
        """Username cannot be get."""
        raise AttributeError

    @username.setter
    def username(self, value):
        """Username cannot be set."""
        raise AttributeError

    @property
    def is_confirmed(self) -> bool:
        """Shortcut to determine if a user has its email verified."""
        return self.email_confirmed

    @property
    def is_admin(self) -> bool:
        """Shortcut to determine if a user is an admin."""
        return self.is_staff and self.is_superuser

    def toggle_admin(self):
        """
        Toggle role between user or administrator.

        For security purpose, also delete its authtokens when the role changes.
        """
        self.is_superuser = not self.is_superuser
        self.is_staff = self.is_superuser

        try:
            self.auth_token.delete()
        except User.auth_token.RelatedObjectDoesNotExist:
            pass

        self.save(update_fields=["is_staff", "is_superuser"])

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []
