"""Website views."""
from copy import deepcopy
import tempfile

from django.db import transaction
from django.db.models.fields.json import KeyTextTransform
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.utils.translation import gettext_lazy as _

from drf_spectacular.utils import extend_schema, OpenApiParameter
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.filters import OrderingFilter
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination

from candelapp.history.manager import LogManager
from candelapp.international.models import Language
from candelapp.website import models
from candelapp.permissions.permissions import ModelPermissions, ModelItemPermissions
from candelapp.website import serializers
from candelapp.website.generate import WebsiteGenerator, GenerationException
from candelapp.drf_url_filters.backend import URLFilterBackend
from candelapp.pagination import CustomPaginationFieldNames

lm = LogManager()


class GenerateAPIView(GenericAPIView):
    """View to generate website based."""

    permission_classes = [IsAdminUser]
    serializer_class = serializers.CFieldSerializer
    queryset = models.CTheme.objects.none()

    def get(self, request, format=None):
        try:
            gen = WebsiteGenerator()
            with tempfile.TemporaryDirectory() as tempdir:
                gen.generate_website(tempdir)
                gen.ftp_command(tempdir)

        except GenerationException as e:
            return Response(
                {"non_field_errors": e.message},
                status=status.HTTP_400_BAD_REQUEST,
            )
        return Response(status=status.HTTP_200_OK)


class CThemeViewSet(viewsets.ModelViewSet):
    """A viewset for viewing and editing page instances."""

    http_method_names = ["get", "post"]
    queryset = models.CTheme.objects.all()
    permission_classes = [IsAdminUser]
    serializer_class = serializers.CThemeSerializer


class CModelCategoryViewSet(viewsets.ModelViewSet):
    """A viewset for managing submenus."""

    queryset = models.CModelCategory.objects.all()
    permission_classes = [IsAdminUser]
    serializer_class = serializers.CModelCategorySerializer
    http_method_names = ["get", "post", "patch", "delete"]


class CModelViewSet(viewsets.ModelViewSet):
    """A viewset for managing models."""

    queryset = models.CModel.objects.all()
    permission_classes = [IsAdminUser | ModelPermissions]
    serializer_class = serializers.CModelSerializer
    http_method_names = ["get", "post", "patch", "delete"]

    @action(detail=False, methods=["get"], name="Available models for pages")
    def available_models_for_pages(self, request):
        """Get all available multiple models not linked to a page."""
        available_models = models.CModel.objects.filter(type="multiple", cpage__isnull=True)
        serializer = self.get_serializer(available_models, many=True)
        return Response(serializer.data)

    @action(
        detail=False,
        methods=["get"],
        name="Get model by internal name",
        url_path=r"get_by_name/(?P<model_name>[\w-]+)",
    )
    def get_by_name(self, request, *args, **kwargs):
        model_name = kwargs.get("model_name")
        model = get_object_or_404(models.CModel, name=model_name)
        self.check_object_permissions(self.request, model)
        serializer = self.get_serializer(model)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        """Override create method to add logging."""
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        lm.add_log_entry(
            type="model_add", instance=instance, user=request.user, new_state=instance.__dict__
        )
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        instance = serializer.save()
        return instance

    def update(self, request, *args, **kwargs):
        """Override update method to add logging."""
        partial = kwargs.pop("partial", False)
        instance = self.get_object()
        prev_state = deepcopy(instance.__dict__)
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        new_state = deepcopy(instance.__dict__)
        lm.add_log_entry(
            type="model_change",
            instance=instance,
            user=request.user,
            prev_state=prev_state,
            new_state=new_state,
        )
        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        """Ensure that model does is not linked to a page or have modelitems."""
        instance = self.get_object()
        if instance.type == "multiple" and instance.cmodelitem_set.exists():
            response = Response(
                data={
                    "non_field_errors": _(
                        "The model cannot be deleted because " "it has objects created with it."
                    )
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        elif instance.cpage_set.exists():
            response = Response(
                data={
                    "non_field_errors": (
                        "The template cannot be deleted because " "it is associated with a page."
                    )
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        else:
            response = Response(status=status.HTTP_204_NO_CONTENT)
            prev_state = deepcopy(instance.__dict__)
            self.perform_destruction(instance)
            lm.add_log_entry(
                type="model_delete",
                instance=instance,
                user=request.user,
                prev_state=prev_state,
            )
        return response

    @transaction.atomic
    def perform_destruction(self, instance):
        """
        Destroy model.

        If model is simple, destroy its modelitems too.
        """
        if instance.type == "simple":
            instance.cmodelitem_set.all().delete()
        self.perform_destroy(instance)


@extend_schema(
    parameters=[
        OpenApiParameter(
            "model",
            {"oneOf": [{"type": "integer"}, {"type": "string"}]},
            location=OpenApiParameter.PATH,
            description="Model identifier. Can be either the pk or the model name",
        )
    ]
)
class CModelItemViewSet(viewsets.ModelViewSet):
    """A viewset for managing CRUD model items."""

    queryset = models.CModelItem.objects.all()
    permission_classes = [IsAdminUser | ModelItemPermissions]
    serializer_class = serializers.CModelItemSerializer
    filter_backends = [OrderingFilter, URLFilterBackend]
    ordering_fields = ["created", "modified"]
    model = None
    modelfield = None
    model_lookup = {"model": {}, "modelitem": {}}
    http_method_names = ["get", "post", "patch", "delete"]

    def initialize_request(self, request, *args, **kwargs):
        """Get model from kwargs before processing the request."""
        self.modelfield = kwargs.get("model")

        # Get model instance with the determined field
        try:
            int(self.modelfield)
            self.model_lookup["model"] = {"pk": self.modelfield}
            self.model_lookup["modelitem"] = {"model__pk": self.modelfield}
        except ValueError:
            self.model_lookup["model"] = {"name": self.modelfield}
            self.model_lookup["modelitem"] = {"model__name": self.modelfield}
        except TypeError:
            # https://drf-spectacular.readthedocs.io/en/latest/faq.html#my-get-queryset-depends-on-some-attributes-not-available-at-schema-generation-time
            if getattr(self, "swagger_fake_view", False):
                return request
            else:
                raise Http404
        try:
            self.model = models.CModel.objects.get(**self.model_lookup["model"])
        except models.CModel.DoesNotExist:
            raise Http404

        return super().initialize_request(request, *args, **kwargs)

    def get_queryset(self):
        """
        Filter queryset with the according model.

        Also annotate when needed jsonfield keys to the queryset so filter and ordering backends
        can reach the jsonfield content.
        """
        queryset = self.queryset.filter(model=self.model)
        can_view_perm = self.model.can_view

        if self.request.user.is_staff or self.request.method == "GET" and self.kwargs.get("pk"):
            pass
        elif self.request.user.is_authenticated:
            if can_view_perm.get("groups_own_objects", False):
                user_groups = self.request.user.groups.values_list(flat=True)
                queryset = queryset.filter(groups__in=list(user_groups))
            elif can_view_perm.get("allow_everyone", False):
                pass
            elif can_view_perm.get("allow_creator", False):
                queryset = queryset.filter(creator_id=self.request.user.id)
            

        # To avoid using the annotation everytime, only use it when there are query params
        # such as 'ordering' or fields lookups
        if self.request.query_params:
            jsonfield_keys_to_annotate = {}
            # Add title and description to model fields
            model_fields = [f["internal_name"] for f in self.model.fields]
            model_fields += ("title", "description")
            for field in self.model.fields:
                name = field.get("internal_name")
                jsonfield_keys_to_annotate.update({name: KeyTextTransform(name, "data")})
            self.ordering_fields += model_fields
            queryset = queryset.annotate(**jsonfield_keys_to_annotate)

        return queryset

    def get_serializer_context(self):
        """Add model attribute to context."""
        context = super().get_serializer_context()
        context["model_attribute"] = self.modelfield
        context["request"] = self.request
        return context

    def list(self, request, *args, **kwargs):
        """
        List modelitems.

        Modelitems can be filtered by using query params.

        Ex: /?one_field=1&another_field=hello will be transformed as such:
        WHERE one_field LIKE 1 or another_field LIKE hello

        Multiple values on the same field has to be separated by a comma or dashes.
        Comma separated values will be considered will use the OR operand.
        Dash will be considered as BETWEEN.

        Ex: /?field=1,2 will be transformed as such:

        WHERE field LIKE 1 OR field like 2

        Range values are separated by dashes.

        Ex: /?field__range=1-2 will be transformed as such:
        WHERE field BETWEEN 1 AND 2

        Chaining is possible.

        Ex: /?field=1-2 will&another_field=hello,bye be transformed as such:
        WHERE field BETWEEN 1 and 2
        AND another_field LIKE hello or another_field LIKE bye

        Warning: __limit and __offset are internal query params used for pagination.
        They won't be available as filterable params.
        """
        return super().list(request, args, kwargs)

    @action(detail=False, methods=["get"], name="Get parent pages")
    def get_parents(self, request, model=None):
        """
        List only parent modelitems.

        Modelitems can be filtered by using query params.

        Ex: /?one_field=1&another_field=hello will be transformed as such:
        WHERE one_field LIKE 1 or another_field LIKE hello

        Multiple values on the same field has to be separated by a comma or dashes.
        Comma separated values will be considered will use the OR operand.
        Dash will be considered as BETWEEN.

        Ex: /?field=1,2 will be transformed as such:

        WHERE field LIKE 1 OR field like 2

        Range values are separated by dashes.

        Ex: /?field__range=1-2 will be transformed as such:
        WHERE field BETWEEN 1 AND 2

        Chaining is possible.

        Ex: /?field=1-2 will&another_field=hello,bye be transformed as such:
        WHERE field BETWEEN 1 and 2
        AND another_field LIKE hello or another_field LIKE bye

        Warning: __limit and __offset are internal query params used for pagination.
        They won't be available as filterable params.
        """
        parent_items = self.filter_queryset(self.get_queryset()).filter(parent__isnull=True)
        serializer = self.get_serializer(parent_items, many=True)
        return Response(serializer.data)

    @action(detail=False, methods=["get"], url_path=r'lang/(?P<lang_code>[\w-]+)')
    def lang(self, request, lang_code=None, **kwargs):
        """
        Get all the items from a model with the specified language code.
        Apply filtering and pagination.
        """
        # Optionally, use kwargs if needed, for example:
        # model_name_or_id = kwargs.get('model')

        queryset = self.filter_queryset(self.get_queryset().filter(lang__code=lang_code))

        # Apply pagination
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        # If pagination is not applied for some reason, fall back to a regular response
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


    @action(
        detail=True,
        methods=["get"],
        name="Get child modelitem from parent",
        url_path=r"get_child/(?P<lang_code>[\w-]+)",
    )
    def get_child(self, request, pk=None, model=None, lang_code=None):
        """
        Get child modelitem from a parent and lang code.

        If a child can't exist because of not existing lang or parent,
        return a 404

        If a child may exist because of existing lang and parent,
        return a 204
        """
        queryset = self.filter_queryset(self.get_queryset())
        if model and lang_code:
            try:
                child_item = queryset.get(
                    **self.model_lookup["modelitem"], parent__pk=pk, lang__code=lang_code
                )
            except models.CModelItem.DoesNotExist:
                child_item = None
            if child_item:
                self.check_object_permissions(self.request, child_item)
                serializer = self.get_serializer(child_item)
                return Response(serializer.data)
            else:
                parentExists = queryset.filter(pk=pk).exists()
                langExists = Language.objects.filter(code=lang_code).exists()
                if parentExists and langExists:
                    return Response(status=status.HTTP_204_NO_CONTENT)
        return Response(status=status.HTTP_404_NOT_FOUND)

    def create(self, request, *args, **kwargs):
        """Override create method to add logging."""
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        lm.add_log_entry(
            type="modelitem_add", instance=instance, user=request.user, new_state=instance.__dict__
        )
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        instance = serializer.save()
        return instance

    def update(self, request, *args, **kwargs):
        """Override update method to add logging."""
        partial = kwargs.pop("partial", False)
        instance = self.get_object()
        prev_state = deepcopy(instance.__dict__)
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        new_state = deepcopy(instance.__dict__)
        lm.add_log_entry(
            type="modelitem_change",
            instance=instance,
            user=request.user,
            prev_state=prev_state,
            new_state=new_state,
        )
        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    @transaction.atomic
    def destroy(self, request, *args, **kwargs):
        """
        Delete parent modelitem.

        Cascade delete all the parent's children.
        """
        instance = self.get_object()
        prev_state = deepcopy(instance.__dict__)
        # Perform a deletion can only be initiated with a parent
        if instance.parent is not None:
            return Response(status=status.HTTP_403_FORBIDDEN)

        children = self.queryset.filter(parent=instance)
        if children:
            children.delete()

        self.perform_destroy(instance)
        lm.add_log_entry(
            type="modelitem_delete",
            instance=instance,
            user=request.user,
            prev_state=prev_state,
        )
        return Response(status=status.HTTP_204_NO_CONTENT)


class CPageViewSet(viewsets.ModelViewSet):
    """A viewset for managing pages."""

    queryset = models.CPage.objects.all()
    permission_classes = [IsAdminUser]
    serializer_class = serializers.CPageReadSerializer
    http_method_names = ["get", "post", "patch", "delete"]

    def get_serializer_class(self):
        """Get serializer depending of the type of request."""
        self.serializer_class = None
        if self.action in ["retrieve", "list", "get_parents", "get_child"]:
            self.serializer_class = serializers.CPageReadSerializer
        elif self.action in ["partial_update", "destroy"]:
            self.serializer_class = serializers.CPageUpdateSerializer
        elif self.action == "create":
            self.serializer_class = serializers.CPageCreateSerializer

        return super().get_serializer_class()

    @transaction.atomic
    def destroy(self, request, *args, **kwargs):
        """
        Delete parent page.

        Cascade delete all the parent's children.
        """
        instance = self.get_object()
        # Perform a deletion can only be initiated with a parent
        if instance.parent is not None:
            return Response(status=status.HTTP_403_FORBIDDEN)

        children = models.CPage.objects.filter(parent=instance)
        if children:
            children.delete()

        self.perform_destroy(instance)

        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=False, methods=["get"], name="Get parent pages")
    def get_parents(self, request):
        """Get only parent pages."""
        parent_pages = self.queryset.filter(parent__isnull=True)
        serializer = self.get_serializer(parent_pages, many=True)
        return Response(serializer.data)

    @action(
        detail=True,
        methods=["get"],
        name="Get child pages from parent",
        url_path=r"get_child/(?P<lang_code>[\w-]+)",
    )
    def get_child(self, request, pk=None, lang_code=None):
        """
        Get child page from a parent and lang code.

        If a child can't exist because of not existing lang or parent,
        return a 404

        If a child may exist because of existing lang and parent,
        return a 204
        """
        valid_pk = False
        try:
            int(pk)
            valid_pk = True
        except ValueError:
            pass

        if all([valid_pk, type(lang_code) == str]):
            try:
                child_page = models.CPage.objects.get(parent__pk=pk, lang__code=lang_code)
            except models.CPage.DoesNotExist:
                child_page = None
            if child_page:
                serializer = self.get_serializer(child_page)
                return Response(serializer.data)
            else:
                parentExists = models.CPage.objects.filter(pk=pk).exists()
                langExists = Language.objects.filter(code=lang_code).exists()
                if all([parentExists, langExists]):
                    return Response(status=status.HTTP_204_NO_CONTENT)
        return Response(status=status.HTTP_404_NOT_FOUND)


class CImageViewSet(viewsets.ModelViewSet):
    """A viewset for managing images."""

    http_method_names = ["get", "post"]
    queryset = models.CImage.objects.all()
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.CImageSerializer


class CFileViewSet(viewsets.ModelViewSet):
    """A viewset for managing files."""

    http_method_names = ["get", "post"]
    queryset = models.CFile.objects.all()
    permission_classes = [IsAdminUser]
    serializer_class = serializers.CFileSerializer
