"""Data fields serializers."""
from typing import List
import re

from django.core.validators import RegexValidator
from django.utils.translation import gettext_lazy as _

from drf_spectacular.utils import extend_schema_field
from drf_spectacular.types import OpenApiTypes
from rest_framework import serializers

from candelapp.website.models import CModel, CModelItem


hex_regex = "^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$"


class InternalNameField(serializers.SlugField):
    """
    Internal name field validator for model fields.
    Behave exactly like a SlugField but disallow the hyphen.
    """

    def __init__(self, allow_unicode=False, **kwargs):
        super().__init__(**kwargs)
        validator = RegexValidator(
            re.compile(r"^[a-zA-Z0-9_]+$"),
            message=self.error_messages["invalid"],
        )
        self.validators.append(validator)


class ColorField(serializers.Field):
    """Hexadecimal objects are serialized into '#fff' or '#ffffff' notation."""

    def to_internal_value(self, data):
        if not isinstance(data, str):
            raise serializers.ValidationError(
                f"Incorrect type. Expected a string but got {type(data).__name__}"
            )

        if not re.search(hex_regex, data):
            raise serializers.ValidationError(_("Not a valid hexadecimal code."))
        return data

    def to_representation(self, value):
        return value


class ChoiceField(serializers.Field):
    """Field to serialize and unserialize choices."""

    @property
    def model_instance(self):
        """Get CModel instance from parent serializer."""
        return self.parent.context["model"]

    @property
    def datafield(self):
        """
        Search for the field in model.fields with the provided
        field name from serializer.

        Ex:
            - Field name is 'fruit_choices'
            - model fields are:
                [
                    {
                        "type": "choice", "internal_name": fruit_choices",
                        choices: ["a", "b"]
                    },
                    {"type": "text", "internal_name": "title"}
                ]

        Will return  {
            "type": "choice", "internal_name": fruit_choices", choices: ["a", "b"]
        }
        """
        return next(
            item for item in self.model_instance.fields if item["internal_name"] == self.field_name
        )

    def to_internal_value(self, data: str) -> str:
        if not isinstance(data, str):
            raise serializers.ValidationError(
                _(f"Incorrect type. Expected a string but got {type(data).__name__}")
            )
        if data not in self.datafield["choices"]:
            raise serializers.ValidationError(_("Invalid choice."))
        return data

    def to_representation(self, value):
        return value


class MultipleChoiceField(ChoiceField):
    """ChoiceField but with multiple values."""

    def to_internal_value(self, data):
        if not isinstance(data, list):
            raise serializers.ValidationError(
                _(f"Incorrect type. Expected a list but got {type(data).__name__}")
            )

        if len(set(data) - set(self.datafield["choices"])) != 0:
            raise serializers.ValidationError(_("Invalid choices."))
        return data

    def to_representation(self, value):
        return value


class ManyToOneField(serializers.Field):
    """
    Field to serialize a many to one relationship.
    It is used when model items are being edited.
    """

    def to_internal_value(self, data):
        if not isinstance(data, list):
            raise serializers.ValidationError(
                _(f"Incorrect type. Expected a list of primary keys but got {type(data).__name__}")
            )
        for item in data:
            if not isinstance(item, int):
                raise serializers.ValidationError(
                    _(
                        f"Incorrect type. Expected a list of primary keys but got {type(item).__name__}"  # NOQA
                    )
                )

        modelitem_lang = None
        if self.context["instance"]:
            modelitem_lang = self.context["instance"].lang
        else:
            modelitem_lang = self.context["lang"]
        if len(data) != CModelItem.objects.filter(pk__in=data, lang=modelitem_lang).count():
            raise serializers.ValidationError(
                _(
                    "One or more specified objects do not exist or "
                    "are not in the same language as the modelitem."
                )
            )

    def to_representation(self, value):
        return value


class OneToManyField(serializers.Field):
    """
    Field to serialize candelapp a one to many relationship.
    """

    def to_internal_value(self, data):
        if not isinstance(data, int):
            raise serializers.ValidationError(
                _(f"Incorrect type. Expected an int but got {type(data).__name__}")
            )
        item = None
        try:
            item = CModelItem.objects.get(pk=data)
        except CModelItem.objects.DoesNotExist:
            raise serializers.ValidationError(_("The modelitem does not exist."))

        modelitem_lang = None
        if self.context["instance"]:
            modelitem_lang = self.context["instance"].lang
        else:
            modelitem_lang = self.context["lang"]
        if modelitem_lang != item.lang:
            raise serializers.ValidationError(
                _("The lang of the relation does not match with the lang of the modelitem.")
            )

    def to_representation(self, value):
        return value


class RelationTypeField(serializers.Field):
    """
    It is used when a ManyToOne or a OneToMany field is defined in the model creation.
    """

    def to_internal_value(self, pk):
        if not CModel.objects.filter(pk=pk).exists():
            raise serializers.ValidationError(
                _("The model used for the relationship does not exist.")
            )
        return pk

    def to_representation(self, value):
        return value


@extend_schema_field(OpenApiTypes.OBJECT)
class ExtrasField(serializers.Field):
    """Field to serialize and unserialize extras values."""

    def to_internal_value(self, data: list) -> List[dict]:
        if not isinstance(data, list):
            raise serializers.ValidationError(
                _(f"Incorrect type. Expected a list but got {type(data).__name__}"),
            )
        for item in data:
            if not isinstance(item, dict):
                raise serializers.ValidationError(
                    _(f"Incorrect type. Expected a list of dict but got {type(data).__name__}"),
                )
            # If item is empty, stop the current loop
            if not item:
                continue
            keys = item.keys()
            if "value" not in keys or "key" not in keys or len(keys) != 2:
                raise serializers.ValidationError(_("Incorrect format. Expected a key value dict."))

            key = item.get("key")
            value = item.get("value")
            if not isinstance(key, str) or not isinstance(value, str):
                raise serializers.ValidationError(
                    _("Key or value has an incorrect type. Expected a string.")
                )
        return data

    def to_representation(self, value) -> List[dict]:
        return value
