"""Generation tests."""
import os
import tempfile
from unittest import mock

from candelapp.authentication.factories import AdminFactory, UserFactory
from candelapp.international.models import Language
from candelapp.settings.models import Settings
from candelapp.tools import test_utils
from candelapp.tools.encryption import PasswordManager
from candelapp.website import factories, tools
from candelapp.website.generate import WebsiteGenerator, GenerationException


ENDPOINTS = {
    "theme": "/api/v1/website/theme/",
}


class GenerationTestCase(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    """Test to generate a website."""

    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        cls.settings = Settings.objects.first()
        cls.settings.site_name = "Candelapp test"
        cls.settings.save(update_fields=["site_name"])
        cls.primary_lang = cls.settings.primary_language
        cls.fr_lang = Language.objects.get(code="fr")
        cls.es_lang = Language.objects.get(code="es")
        cls.settings.secondary_languages.add(cls.fr_lang)
        cls.settings.secondary_languages.add(cls.es_lang)

        cls.admin_user = AdminFactory()
        cls.non_admin_user = UserFactory()

    @mock.patch("zipfile.ZipFile")
    @mock.patch("zipfile.is_zipfile")
    def test_errors(self, mocked_is_zipfile, mocked_archive):
        """Test validation errors."""
        # No site name nor url
        gen = WebsiteGenerator()
        with tempfile.TemporaryDirectory() as tempdir:
            with self.assertRaisesMessage(
                GenerationException,
                ("The site name and address are not configured."),
            ):
                gen.generate_website(tempdir)

        self.settings.site_name = "Fsociety"
        self.settings.site_url = "fsociety.localhost"
        self.settings.save(update_fields=["site_name", "site_url"])
        # No theme
        gen = WebsiteGenerator()
        with tempfile.TemporaryDirectory() as tempdir:
            with self.assertRaisesMessage(GenerationException, "No themes have been imported yet."):
                gen.generate_website(tempdir)

        # Theme but ftp settings not set
        mocked_is_zipfile.return_value = True
        mocked_archive.return_value.namelist.return_value = [
            "templates/",
            "templates/header.html",
            "templates/base.html",
            "templates/footer.html",
        ]
        data = {"file": mocked_archive}
        self._test_post(ENDPOINTS["theme"], self.admin_user, 201, data)

        gen = WebsiteGenerator()
        with tempfile.TemporaryDirectory() as tempdir:
            with self.assertRaisesMessage(
                GenerationException,
                "The FTP settings are not properly configured.",
            ):
                gen.generate_website(tempdir)

        # Set host and user but not password
        self.settings.ftp_host = "localhost"
        self.settings.ftp_user = "user"
        self.settings.save(update_fields=["ftp_host", "ftp_user"])

        gen = WebsiteGenerator()
        with tempfile.TemporaryDirectory() as tempdir:
            with self.assertRaisesMessage(
                GenerationException,
                "The FTP settings are not properly configured.",
            ):
                gen.generate_website(tempdir)

        # Set password and page check
        with mock.patch("ftplib.FTP"):
            pm = PasswordManager()
            self.settings.ftp_password = pm.encrypt("password")
            self.settings.save(update_fields=["ftp_password"])

            gen = WebsiteGenerator()
            with tempfile.TemporaryDirectory() as tempdir:
                with self.assertRaisesMessage(
                    GenerationException,
                    "You need at least one page to generate the site.",
                ):
                    gen.generate_website(tempdir)

        # Create a page with a non existing template
        with mock.patch("ftplib.FTP"):
            factories.CPageFactory(template="non_existing_template.html")
            gen = WebsiteGenerator()
            with tempfile.TemporaryDirectory() as tempdir:
                with self.assertRaisesMessage(
                    GenerationException,
                    (
                        "This or these templates are used in pages but "
                        "do not exist in the current theme: "
                        "['non_existing_template.html']"
                    ),
                ):
                    gen.generate_website(tempdir)

    @mock.patch("ftplib.FTP")
    def test_generation(self, mocked_ftp):
        """Test the generation with real archive and check to file level."""

        # Setup settings, models, modelitems and pages
        ##############################################
        pm = PasswordManager()
        self.settings.ftp_host = "localhost"
        self.settings.ftp_user = "user"
        self.settings.ftp_password = pm.encrypt("password")
        self.settings.site_name = "Fsociety"
        self.settings.site_url = "fsociety.localhost"
        self.settings.save(
            update_fields=[
                "ftp_host",
                "ftp_user",
                "ftp_password",
                "site_name",
                "site_url",
            ]
        )

        # Setup theme
        #############
        current_dir = os.getcwd()
        path = os.path.join(current_dir, "tests", "theme", "theme.zip")

        with open(path, "rb") as zipfile:
            data = {"file": zipfile}
            self._test_post(ENDPOINTS["theme"], self.admin_user, 201, data)

        # Model
        #######
        blogposts_model = factories.CModelFactory(verbose_name="blogposts", type="multiple")

        # Pages
        #######

        # blogpost page list en
        blogpost_page_list_en = factories.CPageFactory(
            title="blog",
            template="blogposts_list.html",
            lang=self.primary_lang,
        )
        # blogpost page list fr
        blogpost_page_list_fr = factories.CPageFactory(
            title="blog",
            template="blogposts_list.html",
            lang=self.fr_lang,
            parent=blogpost_page_list_en,
        )

        # blogpost page detail en
        blogpost_page_detail_en = factories.CPageFactory(
            title="blogposts",
            template="blogpost_detail_en.html",
            lang=self.primary_lang,
            model=blogposts_model,
        )

        # blogpost page detail fr
        factories.CPageFactory(
            title="articles de blog",
            template="blogpost_detail_fr.html",
            lang=self.fr_lang,
            model=blogposts_model,
            parent=blogpost_page_detail_en,
        )

        # Home en
        home_en = factories.CPageFactory(
            title="home",
            template="home.html",
            lang=self.primary_lang,
        )

        # Home fr
        factories.CPageFactory(
            title="home",
            template="home.html",
            lang=self.fr_lang,
            parent=home_en,
        )

        # Contact en
        contact_en = factories.CPageFactory(
            title="home",
            template="contact.html",
            lang=self.primary_lang,
        )

        # Contact fr
        factories.CPageFactory(
            title="home",
            template="contact.html",
            lang=self.fr_lang,
            parent=contact_en,
        )

        # Modelitems
        ############
        blogpost_cm1_en = factories.CModelItemFactory(model=blogposts_model, lang=self.primary_lang)
        blogpost_cm1_fr = factories.CModelItemFactory(
            model=blogposts_model, lang=self.fr_lang, parent=blogpost_cm1_en
        )
        blogpost_cm2_en = factories.CModelItemFactory(model=blogposts_model, lang=self.primary_lang)
        blogpost_cm2_es = factories.CModelItemFactory(
            model=blogposts_model, lang=self.es_lang, parent=blogpost_cm2_en
        )

        # Generate website in tempdir
        #############################
        with tempfile.TemporaryDirectory() as tempdir:
            gen = WebsiteGenerator()
            gen.generate_website(tempdir)

            # Blogpost en
            assert os.path.exists("{}{}/{}".format(tempdir, blogpost_cm1_en.url, "index.html"))

            # Blogpost fr
            assert os.path.exists("{}{}/{}".format(tempdir, blogpost_cm1_fr.url, "index.html"))

            # Blogpost 2 en
            assert os.path.exists("{}{}/{}".format(tempdir, blogpost_cm2_en.url, "index.html"))

            # Blogpost 2 es can't exist as there is no page linked
            assert (
                os.path.exists("{}{}/{}".format(tempdir, blogpost_cm2_es.url, "index.html"))
                is False
            )

            # Blogpost list en
            assert os.path.exists(
                "{}{}/{}".format(
                    tempdir,
                    tools.build_page_url(blogpost_page_list_en, self.settings.primary_language),
                    "index.html",
                )
            )

            # Blogpost list fr
            assert os.path.exists(
                "{}{}/{}".format(
                    tempdir,
                    tools.build_page_url(blogpost_page_list_fr, self.settings.primary_language),
                    "index.html",
                )
            )

            # Assets
            assert os.path.exists(
                "{}/{}".format(
                    tempdir,
                    "assets/",
                )
            )

            # Sitemap
            assert os.path.exists(
                "{}/{}".format(
                    tempdir,
                    "sitemap.xml",
                )
            )

            # Sitemapindex
            assert os.path.exists(
                "{}/{}".format(
                    tempdir,
                    "sitemapindex.xml",
                )
            )

            # Sitemapindex
            assert os.path.exists(
                "{}/{}".format(
                    tempdir,
                    "robots.txt",
                )
            )
