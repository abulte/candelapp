"""Model serializer tests."""
from copy import copy

from rest_framework.exceptions import ErrorDetail

from candelapp.website import constants
from candelapp.website.factories import CModelFactory
from candelapp.website.serializers import CModelSerializer
from candelapp.tools import test_utils


class CModelSerializerTest(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):

    """Test CModelSerializer."""

    def test_no_data(self):
        # No data
        serializer = CModelSerializer(data={})
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "fields": [ErrorDetail(string="This field is required.", code="required")],
            "verbose_name": [ErrorDetail(string="This field is required.", code="required")],
        }

    def test_list_fields_type(self):
        # Fields is a list field
        data = {"verbose_name": "Model", "fields": {"test": "test"}}
        serializer = CModelSerializer(data=data)
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "fields": [
                ErrorDetail(
                    string="A list is required for the model fields definition.",
                    code="invalid",
                )
            ]
        }

    def test_no_name_nor_type(self):
        # Model field has no name
        data = {
            "verbose_name": "Model",
            "fields": [{"required": True, "type": "text"}],
        }
        serializer = CModelSerializer(data=data)
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "fields": {
                "verbose_name": [ErrorDetail(string="This field is required.", code="required")]
            }
        }
        # No type
        data = {
            "verbose_name": "Model",
            "fields": [{"required": True, "verbose_name": "sup"}],
        }
        serializer = CModelSerializer(data=data)
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "fields": {"type": [ErrorDetail(string="This field is required.", code="required")]}
        }

        # No requirement
        data = {
            "verbose_name": "Model",
            "fields": [{"type": "text", "verbose_name": "sup"}],
        }
        serializer = CModelSerializer(data=data)
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "fields": {"required": [ErrorDetail(string="This field is required.", code="required")]}
        }

    def test_model_types(self):
        # Non existing data field type
        data = {
            "verbose_name": "Model",
            "fields": [{"verbose_name": "field1", "required": True, "type": "decimal"}],
        }
        serializer = CModelSerializer(data=data)
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "fields": {
                "type": [
                    ErrorDetail(string='"decimal" is not a valid choice.', code="invalid_choice")
                ]
            }
        }

        # Ensure all the allowed types work
        model = CModelFactory()
        for _type in constants.DATA_FIELD_TYPES:
            data = {
                "verbose_name": "Model",
                "fields": [{"verbose_name": "field1", "required": True, "type": _type}],
            }
            match _type:
                case "choice" | "multiplechoice":
                    data["fields"][0]["choices"] = ["cat", "dog"]
                case "manytoone":
                    data["fields"][0]["model"] = model.pk
            serializer = CModelSerializer(data=data)
            serializer.is_valid() is True

    def test_reserved_names(self):
        # Ensure no reserved fields are used
        for reserved_field in constants.RESERVED_FIELD_NAMES:
            data = {
                "verbose_name": "Model",
                "fields": [
                    {
                        "internal_name": reserved_field,
                        "required": False,
                        "type": "text",
                        "verbose_name": "hey",
                    }
                ],
            }
            serializer = CModelSerializer(data=data)
            assert serializer.is_valid() is False
            assert serializer.errors == {
                "fields": [
                    ErrorDetail(
                        string="Some field names are internal and reserved so cannot be used: "
                        "('created', 'data', 'modified', 'name', 'type', 'verbose_name', "
                        "'url', 'category_id', 'id', 'parent_id', 'lang_id')",
                        code="invalid",
                    )
                ]
            }
        # ChoiceField and MultipleChoiceField have specific content
        # ChoiceField
        data = {
            "verbose_name": "Model",
            "fields": [{"verbose_name": "Choice field", "required": True, "type": "choice"}],
        }
        serializer = CModelSerializer(data=data)
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "fields": {"choices": [ErrorDetail(string="This field is required.", code="required")]}
        }
        # MultipleChoiceField
        data = {
            "verbose_name": "Model",
            "fields": [
                {
                    "verbose_name": "Multiple choice field",
                    "required": False,
                    "type": "multiplechoice",
                }
            ],
        }
        serializer = CModelSerializer(data=data)
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "fields": {"choices": [ErrorDetail(string="This field is required.", code="required")]}
        }

        # Unique fields name
        data = {
            "verbose_name": "Model",
            "fields": [
                {"verbose_name": "field1", "required": False, "type": "text"},
                {"verbose_name": "field1", "required": False, "type": "text"},
            ],
        }
        serializer = CModelSerializer(data=data)
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "fields": [
                ErrorDetail(
                    string="Two fields have the same or too similar name.",
                    code="invalid",
                )
            ]
        }

        # Good payload with all data fields types
        another_model = CModelFactory()
        data = {
            "verbose_name": "Model",
            "category": None,
            "fields": [
                {
                    "internal_name": "title",
                    "verbose_name": "Title",
                    "required": True,
                    "type": "text",
                    "extras": [{"key": "value", "value": "value"}],
                },
                {
                    "verbose_name": "description",
                    "required": False,
                    "type": "text",
                    "extras": [{"key": "value", "value": "value"}],
                },
                {
                    "verbose_name": "choicefield",
                    "required": False,
                    "type": "choice",
                    "choices": ["1", "2"],
                    "extras": [{"key": "value", "value": "value"}],
                },
                {"verbose_name": "colorfield", "required": False, "type": "color"},
                {"verbose_name": "filefield", "required": False, "type": "file"},
                {
                    "verbose_name": "manytoonefield",
                    "required": False,
                    "type": "manytoone",
                    "model": another_model.pk,
                },
                {"verbose_name": "imagefield", "required": False, "type": "image"},
                {
                    "verbose_name": "multiplechoicefield",
                    "required": True,
                    "type": "multiplechoice",
                    "choices": ["1", "2"],
                },
                {"verbose_name": "numberfield", "required": False, "type": "number"},
                {"verbose_name": "textfield", "required": False, "type": "text"},
                {"verbose_name": "wysiwygfield", "required": False, "type": "wysiwyg"},
            ],
        }
        serializer = CModelSerializer(data=data)
        assert serializer.is_valid()
        serializer.save()

    def test_manytoone_field(self):
        """Test when manytoone field is used."""
        # When using a manytoone field, "model" parameter has to be specified
        data = {
            "verbose_name": "Model",
            "fields": [
                {
                    "verbose_name": "field1",
                    "required": False,
                    "type": "manytoone",
                },
            ],
        }
        serializer = CModelSerializer(data=data)
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "fields": {"model": [ErrorDetail(string="This field is required.", code="required")]}
        }

        # Unknown model
        data = {
            "verbose_name": "Model",
            "fields": [
                {
                    "verbose_name": "field1",
                    "required": False,
                    "type": "manytoone",
                    "model": 42,
                },
            ],
        }
        serializer = CModelSerializer(data=data)
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "fields": {
                "model": [
                    ErrorDetail(
                        string="The model used for the relationship does not exist.",
                        code="invalid",
                    )
                ]
            }
        }

        # Ok
        model = CModelFactory(verbose_name="model_test")
        fields = copy(test_utils.initial_fields)
        fields.append(
            {
                "verbose_name": "field1",
                "required": False,
                "type": "manytoone",
                "model": model.pk,
            }
        )
        data = {"verbose_name": "Model", "fields": fields}

        serializer = CModelSerializer(data=data)
        assert serializer.is_valid() is True

    def test_model_extras_field(self):
        """Test extras property from a model."""
        # Extras is a list of dicts
        data = {
            "verbose_name": "Model",
            "fields": [
                {
                    "verbose_name": "field1",
                    "required": False,
                    "type": "text",
                    "extras": 2,
                },
            ],
        }
        serializer = CModelSerializer(data=data)
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "fields": {
                "extras": [
                    ErrorDetail(
                        string="Incorrect type. Expected a list but got int",
                        code="invalid",
                    )
                ]
            }
        }

        # One extras item must be a dict
        data = {
            "verbose_name": "Model",
            "fields": [
                {
                    "verbose_name": "field1",
                    "required": False,
                    "type": "text",
                    "extras": [2, 2],
                },
            ],
        }
        serializer = CModelSerializer(data=data)
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "fields": {
                "extras": [
                    ErrorDetail(
                        string="Incorrect type. Expected a list of dict but got list",
                        code="invalid",
                    )
                ]
            }
        }

        # Extras dict has to be a key/value one
        data = {
            "verbose_name": "Model",
            "fields": [
                {
                    "verbose_name": "field1",
                    "required": False,
                    "type": "text",
                    "extras": [{"key": "key", "value": "value", "hey": "hey"}],
                }
            ],
        }
        serializer = CModelSerializer(data=data)
        assert serializer.is_valid() is False

        assert serializer.errors == {
            "fields": {
                "extras": [
                    ErrorDetail(
                        string="Incorrect format. Expected a key value dict.",
                        code="invalid",
                    )
                ]
            }
        }

        # Extras keys and values must be strings
        data = {
            "verbose_name": "Model",
            "fields": [
                {
                    "verbose_name": "field1",
                    "required": False,
                    "type": "text",
                    "extras": [{"key": 2, "value": "value"}],
                },
            ],
        }
        serializer = CModelSerializer(data=data)
        assert serializer.is_valid() is False

        assert serializer.errors == {
            "fields": {
                "extras": [
                    ErrorDetail(
                        string="Key or value has an incorrect type. Expected a string.",
                        code="invalid",
                    )
                ]
            }
        }

        # OK
        fields = copy(test_utils.initial_fields)
        fields.append(
            {
                "verbose_name": "field1",
                "required": False,
                "type": "text",
                "extras": [{"key": "key", "value": "value"}],
            },
        )

        data = {
            "verbose_name": "Model",
            "fields": fields,
        }
        serializer = CModelSerializer(data=data)
        assert serializer.is_valid() is True

    def test_update_model_names_and_type(self):
        """Test that model name can't be updated."""
        # With a model
        model = CModelFactory(verbose_name="Model Test")

        # By trying to update the internal name
        data = {"name": "updatedname"}
        serializer = CModelSerializer(data=data, instance=model, partial=True)
        assert serializer.is_valid() is True

        # It should not change
        assert serializer.instance.name == "model_test"

        # By updating the verbose_name
        data = {"verbose_name": "updatedname"}
        serializer = CModelSerializer(data=data, instance=model, partial=True)

        # It should raise the according error
        data = {"type": "multiple"}
        serializer = CModelSerializer(data=data, instance=model, partial=True)
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "type": [ErrorDetail(string="The model type can't be updated.", code="invalid")]
        }
