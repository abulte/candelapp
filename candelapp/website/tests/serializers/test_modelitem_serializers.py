"""ModelItem serializer tests."""
import pytz

from django.conf import settings
from django.core.exceptions import ValidationError

from rest_framework.exceptions import ErrorDetail

from candelapp.authentication.factories import UserFactory
from candelapp.groups.factories import GroupFactory
from candelapp.international.models import Language
from candelapp.settings.models import Settings
from candelapp.website import factories
from candelapp.website import models
from candelapp.website.serializers import CModelItemSerializer

from candelapp.tools import test_utils


TZ = pytz.timezone(settings.TIME_ZONE)


class CModelItemSerializerTest(test_utils.APITestCaseWithData):

    """Test CModelItemSerializer."""

    @classmethod
    def setup_class(cls):
        super().setUpTestData()
        settings = Settings.objects.first()
        cls.primary_lang = settings.primary_language
        cls.cv_lang = Language.objects.get(code="cv")
        cls.es_lang = Language.objects.get(code="es")
        cls.fi_lang = Language.objects.get(code="fi")
        settings.secondary_languages.add(cls.cv_lang, cls.es_lang, cls.fi_lang)

    def test_serializer_modelitem(self):
        """Test serializer."""
        # No data
        serializer = CModelItemSerializer(data={}, context={"model_attribute": "products"})
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "data": [ErrorDetail(string="This field is required.", code="required")],
            "lang": [ErrorDetail(string="This field is required.", code="required")],
        }

        # No lang
        data = {"data": {"name": "Name"}}
        serializer = CModelItemSerializer(data={}, context={"model_attribute": "products"})
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "data": [ErrorDetail(string="This field is required.", code="required")],
            "lang": [ErrorDetail(string="This field is required.", code="required")],
        }

        # Model does not exist
        data = {"data": {"name": "Name"}, "lang": self.primary_lang.pk}
        serializer = CModelItemSerializer(data=data, context={"model_attribute": "products"})
        with self.assertRaises(models.CModel.DoesNotExist):
            serializer.is_valid()

        # Model exists but fields are not known
        model = factories.CModelFactory(verbose_name="Products", type="multiple")
        data = {
            "data": {"title": "title", "unknown_field": "hey"},
            "lang": self.primary_lang.pk,
        }
        serializer = CModelItemSerializer(data=data, context={"model_attribute": "products"})

        assert serializer.is_valid() is False
        assert serializer.errors == {
            "unknown_field": [ErrorDetail(string="This field is not known.", code="invalid")]
        }

        # Title is required
        data = {
            "data": {},
            "lang": self.primary_lang.pk,
        }
        serializer = CModelItemSerializer(data=data, context={"model_attribute": "products"})
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "title": [ErrorDetail(string="This field is required.", code="required")]
        }

        # Invalid data
        data = {
            "data": {
                "title": "",
                "description": None,
                "written": "22/06/1994",
                "price": "str",
                "content": "Hello",
                "picture": "https://candelapp.com/gif.gif",
                "invoice": "https://candelapp.com/invoice.pdf",
            },
            "lang": self.primary_lang.pk,
        }
        serializer = CModelItemSerializer(data=data, context={"model_attribute": "products"})
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "title": [ErrorDetail(string="This field may not be blank.", code="blank")],
            "price": [ErrorDetail(string="A valid number is required.", code="invalid")],
        }

        # Unknown fields
        data = {
            "data": {
                "title": "Salud",
                "description": "Soy una descripción con 150 caracteres",
                "unknown_field": "hola que tal",
            },
            "lang": self.primary_lang.pk,
        }
        serializer = CModelItemSerializer(data=data, context={"model_attribute": "products"})
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "unknown_field": [ErrorDetail(string="This field is not known.", code="invalid")],
        }
        # Not primary lang
        data = {
            "data": {
                "title": "title of the object",
                "description": "description",
                "written": "1993-12-12",
                "price": 3.4,
                "content": "Hello how low",
                "picture": "https://candelapp.com/gif.gif",
                "invoice": "https://candelapp.com/invoice.pdf",
            },
            "lang": self.cv_lang.pk,
        }
        serializer = CModelItemSerializer(data=data, context={"model_attribute": "products"})
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "lang": [
                ErrorDetail(
                    string="The language of a parent must be the main language.",
                    code="invalid",
                )
            ]
        }

        parent = factories.CModelItemFactory(model=model)
        factories.CModelItemFactory(model=model, parent=parent, lang=self.cv_lang)

        # Can't use already used lang
        data = {
            "data": {
                "title": "title of the object",
                "description": "description",
                "written": "1993-12-12",
                "price": 3.4,
                "content": "Hello how low",
                "picture": "https://candelapp.com/gif.gif",
                "invoice": "https://candelapp.com/invoice.pdf",
            },
            "lang": self.cv_lang.pk,
            "parent": parent.pk,
        }
        serializer = CModelItemSerializer(data=data, context={"model_attribute": "products"})
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "lang": [
                ErrorDetail(
                    string="The language of a child cannot be used by another "
                    "child or be the main language.",
                    code="invalid",
                )
            ]
        }
        # Data ok
        data = {
            "data": {
                "title": "title of the object",
                "description": "description",
                "written": "1993-12-12",
                "price": 3.4,
                "content": "Hello how low",
                "picture": "https://candelapp.com/gif.gif",
                "invoice": "https://candelapp.com/invoice.pdf",
            },
            "lang": self.primary_lang.pk,
        }
        serializer = CModelItemSerializer(data=data, context={"model_attribute": "products"})
        assert serializer.is_valid() is True

        # the model is added in validated data
        assert serializer.validated_data["model"] == model

        instance = serializer.save()

        assert instance.model == model
        assert instance.data.items() - data["data"].items() == set()

        # Update data
        # Update parent or lang won't affect the modelitem
        data = {
            "data": {
                "title": "new title",
                "description": "another description",
                "written": "2012-12-12",
                "price": 15.3,
                "content": "Hey, new content",
                "picture": "https://candelapp.com/gif.gif",
                "invoice": "https://candelapp.com/invoice.pdf",
            },
            "lang": self.es_lang.pk,
            "parent": parent.pk,
        }
        serializer = CModelItemSerializer(
            instance,
            data=data,
            context={"model_attribute": "products"},
            partial=True,
        )
        assert serializer.is_valid() is True
        instance = serializer.save()

        assert instance.data == data["data"]
        assert instance.lang != self.es_lang
        assert instance.parent != parent

    def test_serializer_with_simple_model(self):
        """Serialize a modelitem instance with a simple model."""
        model = factories.CModelFactory(verbose_name="Contact", type="simple")
        cmi_parent = factories.CModelItemFactory(model=model)

        # Data ok with a child and another lang
        data = {
            "data": {
                "title": "title of the object",
                "description": "description",
                "written": "1993-12-12",
                "price": 3.4,
                "content": "Hello how low",
                "picture": "https://candelapp.com/gif.gif",
                "invoice": "https://candelapp.com/invoice.pdf",
            },
            "lang": self.es_lang.pk,
            "parent": cmi_parent.pk,
        }
        serializer = CModelItemSerializer(data=data, context={"model_attribute": "contact"})
        assert serializer.is_valid() is True

        # But it is not with another parent on the same model
        data.pop("parent")
        data["lang"] = self.primary_lang.pk
        serializer = CModelItemSerializer(data=data, context={"model_attribute": "contact"})
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "non_field_errors": [
                ErrorDetail(
                    string="Only one parent can be created with a simple model.",
                    code="invalid",
                )
            ]
        }

    def test_serializer_with_groups(self):
        factories.CModelFactory(verbose_name="blogposts", type="multiple")
        group = GroupFactory()

        data = {
            "data": {
                "title": "title of the object",
                "description": "description",
                "written": "1993-12-12",
                "price": 3.4,
                "content": "Hello how low",
                "picture": "https://candelapp.com/gif.gif",
                "invoice": "https://candelapp.com/invoice.pdf",
            },
            "lang": self.primary_lang.pk,
            "groups": [group.pk],
        }
        serializer = CModelItemSerializer(data=data, context={"model_attribute": "blogposts"})
        assert serializer.is_valid() is True
        instance = serializer.save()
        assert list(instance.groups.all()) == [group]

    def test_colorfield(self):
        model = factories.CModelFactory(verbose_name="colorful model", type="simple")
        model.fields.append(
            {
                "type": "color",
                "verbose_name": "Color",
                "internal_name": "color",
                "required": True,
            }
        )
        model.save()

        # Hex rgb safe
        data = {
            "data": {
                "title": "Color test",
                "description": "This is a colorful test",
                "color": "#fff",
            },
            "lang": self.primary_lang.pk,
        }
        serializer = CModelItemSerializer(data=data, context={"model_attribute": model.name})
        assert serializer.is_valid()

        # Hex rgb
        data["data"]["color"] = "#ffffff"
        serializer = CModelItemSerializer(data=data, context={"model_attribute": model.name})
        assert serializer.is_valid()

        # Invalid
        data["data"]["color"] = "#fffffp"
        serializer = CModelItemSerializer(data=data, context={"model_attribute": model.name})
        assert serializer.is_valid() is False

    def test_choice_field(self):
        model = factories.CModelFactory(type="simple")
        model.fields.append(
            {
                "type": "choice",
                "verbose_name": "Choix",
                "internal_name": "choix",
                "required": False,
                "choices": ["Uno", "Dos", "Tres"],
            }
        )
        model.save()

        # Invalid choice
        data = {
            "data": {
                "title": "Color test",
                "description": "This is a colorful test",
                "choix": "Deux",
            },
            "lang": self.primary_lang.pk,
        }
        serializer = CModelItemSerializer(data=data, context={"model_attribute": model.name})
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "choix": [ErrorDetail(string="Invalid choice.", code="invalid")]
        }
        # Not a str
        data["data"]["choix"] = ["Deux", "3"]
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "choix": [ErrorDetail(string="Invalid choice.", code="invalid")]
        }

        # Good choice
        data["data"]["choix"] = "Uno"
        serializer = CModelItemSerializer(data=data, context={"model_attribute": model.name})
        assert serializer.is_valid()

    def test_multiplechoice_field(self):
        model = factories.CModelFactory(type="simple")
        model.fields.append(
            {
                "type": "multiplechoice",
                "verbose_name": "Choix",
                "internal_name": "choix_multiple",
                "required": False,
                "choices": ["Uno", "Dos", "Tres"],
            }
        )
        model.save()

        # Invalid choices
        data = {
            "data": {
                "title": "Color test",
                "description": "This is a colorful test",
                "choix_multiple": ["Tres", "Quatro"],
            },
            "lang": self.primary_lang.pk,
        }
        serializer = CModelItemSerializer(data=data, context={"model_attribute": model.name})
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "choix_multiple": [ErrorDetail(string="Invalid choices.", code="invalid")]
        }

        # Not a list
        data["data"]["choix_multiple"] = "Tres"
        serializer = CModelItemSerializer(data=data, context={"model_attribute": model.name})
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "choix_multiple": [
                ErrorDetail(
                    string="Incorrect type. Expected a list but got str",
                    code="invalid",
                )
            ]
        }

        # Good choice
        data["data"]["choix_multiple"] = ["Uno"]
        serializer = CModelItemSerializer(data=data, context={"model_attribute": model.name})
        assert serializer.is_valid()

        # Good choices
        data["data"]["choix_multiple"] = ["Uno", "Dos"]
        serializer = CModelItemSerializer(data=data, context={"model_attribute": model.name})
        assert serializer.is_valid()

    def test_manytoone_field(self):
        model = factories.CModelFactory()
        another_model = factories.CModelFactory(type="multiple")
        cmi1 = factories.CModelItemFactory(model=another_model, lang=self.primary_lang)
        cmi2 = factories.CModelItemFactory(model=another_model, lang=self.primary_lang)

        model.fields.append(
            {
                "type": "manytoone",
                "verbose_name": "Ma ny to one",
                "internal_name": "many_to_one",
                "model": another_model.pk,
                "required": False,
            }
        )
        model.save()

        # With another many to one key field with a not existing model
        model.fields.append(
            {
                "type": "manytoone",
                "verbose_name": "another many to one",
                "internal_name": "another_fk",
                "model": 28372983798,
                "required": True,
            }
        )

        with self.assertRaisesMessage(
            ValidationError,
            repr({"model": ["The model used for the relationship does not exist."]}),
        ):
            model.save()

        # Invalid data type
        data = {
            "data": {
                "title": "FK test",
                "description": "This is a test",
                "many_to_one": 2,
            },
            "lang": self.primary_lang.pk,
        }
        serializer = CModelItemSerializer(data=data, context={"model_attribute": model.name})
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "many_to_one": [
                ErrorDetail(
                    string="Incorrect type. Expected a list of primary keys but got int",
                    code="invalid",
                )
            ]
        }

        # Invalid many to one
        data = {
            "data": {
                "title": "FK test",
                "description": "This is a test",
                "many_to_one": ["hey", 2],
            },
            "lang": self.primary_lang.pk,
        }
        serializer = CModelItemSerializer(data=data, context={"model_attribute": model.name})
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "many_to_one": [
                ErrorDetail(
                    string="Incorrect type. Expected a list of primary keys but got str",
                    code="invalid",
                )
            ]
        }

        # Not existing modelitems
        data = {
            "data": {
                "title": "FK test",
                "description": "This is a test",
                "many_to_one": [42, 89],
            },
            "lang": self.primary_lang.pk,
        }
        serializer = CModelItemSerializer(data=data, context={"model_attribute": model.name})
        assert serializer.is_valid() is False
        assert serializer.errors == {
            "many_to_one": [
                ErrorDetail(
                    string=(
                        "One or more specified objects do not exist "
                        "or are not in the same language as the modelitem."
                    ),
                    code="invalid",
                )
            ]
        }

        # Ok
        data = {
            "data": {
                "title": "FK test",
                "description": "This is a test",
                "many_to_one": [cmi1.pk, cmi2.pk],
            },
            "lang": self.primary_lang.pk,
        }
        serializer = CModelItemSerializer(data=data, context={"model_attribute": model.name})
        assert serializer.is_valid()
        serializer.save()

    def test_deserializer(self):
        """Deserialize a modelitem instance."""
        model = factories.CModelFactory(type="multiple", is_creator_visible=False)
        parent_page = factories.CPageFactory(title="Test", lang=self.primary_lang, model=model)
        factories.CPageFactory(
            title="Test child",
            parent=parent_page,
            lang=self.es_lang,
            model=model,
        )

        parent_cmi = factories.CModelItemFactory(model=model, lang=self.primary_lang)
        cmi = factories.CModelItemFactory(
            parent=parent_cmi, model=model, lang=self.es_lang, creator=UserFactory()
        )

        serializer = CModelItemSerializer(cmi)
        assert serializer.data == {
            "pk": cmi.pk,
            "created": cmi.created.astimezone(TZ).isoformat(),
            "modified": cmi.modified.astimezone(TZ).isoformat(),
            "model": model.pk,
            "status": "draft",
            "translated_status": "Draft",
            "groups": [],
            "url": cmi.url,
            "data": cmi.data,
            "parent": parent_cmi.pk,
            "lang": self.es_lang.pk,
            # is_creator_visible = False on model
            "creator": None,
        }

    def test_deserializer_with_creator(self):
        """Deserialize a modelitem instance with a creator-configured model."""
        model = factories.CModelFactory(type="multiple", is_creator_visible=True)
        user = UserFactory()
        cmi = factories.CModelItemFactory(model=model, lang=self.primary_lang, creator=user)

        serializer = CModelItemSerializer(cmi)
        assert serializer.data == {
            "pk": cmi.pk,
            "created": cmi.created.astimezone(TZ).isoformat(),
            "modified": cmi.modified.astimezone(TZ).isoformat(),
            "model": model.pk,
            "status": "draft",
            "translated_status": "Draft",
            "groups": [],
            "url": cmi.url,
            "data": cmi.data,
            "parent": None,
            "lang": self.primary_lang.pk,
            "creator": {
                "pk": user.pk,
                "first_name": user.first_name,
                "last_name": user.last_name,
            },
        }
