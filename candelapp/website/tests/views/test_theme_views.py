"""Theme api tests."""
from io import BytesIO
from unittest import mock
from zipfile import ZipFile, ZipInfo

from django.db import connection
from django.core.files.uploadedfile import SimpleUploadedFile

from candelapp.authentication.factories import AdminFactory, UserFactory
from candelapp.website import models
from candelapp.tools import test_utils


ENDPOINTS = {
    "theme": "/api/v1/website/theme/",
}

in_memory_zip = BytesIO()
in_memory_zip.name = "theme.zip"


class ThemeAPITestCase(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    """Version endpoint testcases."""

    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        cls.admin_user = AdminFactory()
        cls.non_admin_user = UserFactory()
        cls.zip_file = ZipFile(in_memory_zip, mode="w")

    def test_send_with_admin_user(self):
        """Try to send with a admin user."""
        data = {"file": in_memory_zip}
        self._test_post(ENDPOINTS["theme"], self.admin_user, 400, data)

    def test_send_with_non_admin_user(self):
        """Try to send with a non admin user."""
        data = {"file": in_memory_zip}
        self._test_post(ENDPOINTS["theme"], self.non_admin_user, 403, data)

    @mock.patch("zipfile.ZipFile")
    @mock.patch("zipfile.is_zipfile")
    def test_file_validation(self, mocked_is_zipfile, mocked_archive):
        """Test to trigger all the possible errors when uploading the zip file."""

        # Empty payload
        data = {"file": ""}
        response = self._test_post(ENDPOINTS["theme"], self.admin_user, 400, data)
        assert response.json() == {
            "file": ["The submitted data was not a file. Check the encoding type on the form."]
        }
        # Empty file
        data = {"file": in_memory_zip}
        response = self._test_post(ENDPOINTS["theme"], self.admin_user, 400, data)
        assert response.json() == {"file": ["The submitted file is empty."]}

        # Not a zip
        mocked_is_zipfile.return_value = False
        data = {"file": mocked_is_zipfile}
        response = self._test_post(ENDPOINTS["theme"], self.admin_user, 400, data)
        assert response.json() == {"file": ["The file is not a .zip"]}

        # Missing templates folder
        mocked_is_zipfile.return_value = True
        mocked_archive.return_value.namelist.return_value = ["assets/"]
        response = self._test_post(ENDPOINTS["theme"], self.admin_user, 400, data)
        assert response.json() == {
            "file": ["The directory containing the template files is missing."]
        }
        # Missing base templates
        mocked_archive.return_value.namelist.return_value = [
            "templates/",
            "templates/base.html",
            "templates/header.html",
        ]
        response = self._test_post(ENDPOINTS["theme"], self.admin_user, 400, data)
        assert response.json() == {
            "file": [
                (
                    "One or more required templates are missing. "
                    "(base.html, header.html,and footer.html are the minimum requirements)."
                )
            ]
        }

    def test_with_real_archives(self):
        """Send a valid file and check the unzipping process."""
        # Not a .zip
        gif = BytesIO(
            b"GIF87a\x01\x00\x01\x00\x80\x01\x00\x00\x00\x00ccc,\x00"
            b"\x00\x00\x00\x01\x00\x01\x00\x00\x02\x02D\x01\x00;"
        )
        gif.name = "test.gif"
        response = self._test_post(ENDPOINTS["theme"], self.admin_user, 400, {"file": gif})
        assert response.json() == {"file": ["The file is not a .zip"]}

        folders = ["templates/"]
        tpl_files = [
            "templates/base.html",
            "templates/header.html",
            "templates/footer.html",
            "templates/home.html",
            "templates/hello.html",
        ]
        with ZipFile(in_memory_zip, "w") as archive:
            for f in folders:
                zi = ZipInfo(f)
                archive.writestr(zi, "")
            for tpl in tpl_files:
                zi = ZipInfo(tpl)
                archive.writestr(zi, "<some html/>")

        in_memory_zip2 = BytesIO()
        in_memory_zip2.write(in_memory_zip.getbuffer())

        # Too high
        with self.settings(MAX_FILE_UPLOAD_SIZE=1):
            data = {"file": SimpleUploadedFile(name="file.zip", content=in_memory_zip.getbuffer())}
            response = self._test_post(ENDPOINTS["theme"], self.admin_user, 400, data)
            assert response.json() == {"file": ["The file is too big."]}

        # Upload a theme
        data = {"file": SimpleUploadedFile(name="file.zip", content=in_memory_zip.getbuffer())}
        self._test_post(ENDPOINTS["theme"], self.admin_user, 201, data)

        assert models.CTheme.objects.count() == 1

        theme = models.CTheme.objects.first()
        # Base, header and footer are not directly usable so won't
        # be available as templates
        assert theme.templates == [
            "home.html",
            "hello.html",
        ]
        tenant = connection.tenant
        assert (
            "/media/{}/theme/{}".format(tenant.name, theme.file.name.split("/")[-1])
            in theme.file.path
        )

        # Upload another theme
        data = {"file": SimpleUploadedFile(name="file.zip", content=in_memory_zip.getbuffer())}
        self._test_post(ENDPOINTS["theme"], self.admin_user, 201, data)

        # There can be only 1 theme
        assert models.CTheme.objects.count() == 1
