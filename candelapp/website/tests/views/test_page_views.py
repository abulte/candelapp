"""Page api tests."""
from candelapp.authentication.factories import AdminFactory, UserFactory
from candelapp.international.models import Language
from candelapp.settings.models import Settings
from candelapp.tools import test_utils
from candelapp.website import factories, models


ENDPOINTS = {
    "pages": "/api/v1/website/pages/",
    "parent_pages": "/api/v1/website/pages/get_parents/",
    "children_page": "/api/v1/website/pages/{}/get_child/{}/",
}


class CPageAPITestCase(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    """CPage endpoint testcases."""

    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        settings = Settings.objects.first()
        cls.primary_lang = settings.primary_language
        cls.cv_lang = Language.objects.get(code="cv")
        cls.es_lang = Language.objects.get(code="es")
        cls.fi_lang = Language.objects.get(code="fi")
        settings.secondary_languages.add(cls.cv_lang, cls.es_lang, cls.fi_lang)

        cls.admin_user = AdminFactory()
        cls.non_admin_user = UserFactory()
        cls.model = factories.CModelFactory(type="multiple")
        cls.model_simple = factories.CModelFactory(type="simple")
        cls.page = factories.CPageFactory(model=cls.model, lang=cls.primary_lang)

    def test_list_non_admin_user(self):
        """Try to get with a non admin user."""
        response = self._test_get(
            ENDPOINTS["pages"],
            self.non_admin_user,
            403,
        )
        assert response.json() == {"detail": "You do not have permission to perform this action."}

    def test_list_admin_user(self):
        """Try to get with a admin user."""
        response = self._test_get(
            ENDPOINTS["pages"],
            self.admin_user,
            200,
        )
        assert len(response.json()) == 1

    def test_list_pages(self):
        """Test page listing."""
        # Create another parent page
        parent = factories.CPageFactory()

        response = self._test_get(ENDPOINTS["pages"], self.admin_user, 200)
        assert len(response.json()) == 2

        # Create a children page
        factories.CPageFactory(parent=parent, lang=self.es_lang)
        factories.CPageFactory(parent=parent, lang=self.cv_lang)

        response = self._test_get(ENDPOINTS["pages"], self.admin_user, 200)
        assert len(response.json()) == 4

        response = self._test_get(ENDPOINTS["parent_pages"], self.admin_user, 200)
        assert len(response.json()) == 2

    def test_get_pages(self):
        """Test to get pages."""
        # Get page
        parent = factories.CPageFactory()
        response = self._test_get(
            "{}{}/".format(ENDPOINTS["pages"], parent.pk), self.admin_user, 200
        )
        assert parent.pk == response.json()["pk"]

        # Get children pages
        children1 = factories.CPageFactory(parent=parent, lang=self.es_lang)
        children2 = factories.CPageFactory(parent=parent, lang=self.cv_lang)

        # Unknown parent and lang
        response = self._test_get(
            ENDPOINTS["children_page"].format(9999, "zz"), self.admin_user, 404
        )

        # Weird url
        response = self._test_get(
            ENDPOINTS["children_page"].format("undefined", 32),
            self.admin_user,
            404,
        )

        # Parent and lang exists but not child
        response = self._test_get(
            ENDPOINTS["children_page"].format(parent.pk, self.fi_lang.code),
            self.admin_user,
            204,
        )

        # Get children
        response = self._test_get(
            ENDPOINTS["children_page"].format(parent.pk, children1.lang.code),
            self.admin_user,
            200,
        )
        assert response.json()["pk"] == children1.pk

        response = self._test_get(
            ENDPOINTS["children_page"].format(parent.pk, children2.lang.code),
            self.admin_user,
            200,
        )
        assert response.json()["pk"] == children2.pk

    def test_create_page(self):
        """Try to create a page."""
        factories.CThemeFactory(templates=["template.html"])
        # Create page without model
        data = {
            "title": "Hello",
            "description": "Description",
            "type": "simple",
            "template": "template.html",
            "parent": None,
            "lang": self.primary_lang.pk,
        }
        self._test_post(ENDPOINTS["pages"], self.admin_user, 201, data, as_json=True)

        assert models.CPage.objects.count() == 2

        # Create page with a model
        another_model = factories.CModelFactory(type="multiple")
        data = {
            "title": "Hello",
            "type": "multiple",
            "template": "template.html",
            "model": another_model.pk,
            "parent": None,
            "lang": self.primary_lang.pk,
        }
        self._test_post(ENDPOINTS["pages"], self.admin_user, 201, data, as_json=True)

        assert models.CPage.objects.count() == 3

        # Create child from parent without a model
        parent = factories.CPageFactory()
        data = {
            "title": "Hello",
            "description": "desc",
            "template": "template.html",
            "parent": parent.pk,
            "lang": self.es_lang.pk,
        }
        self._test_post(ENDPOINTS["pages"], self.admin_user, 201, data, as_json=True)
        page = models.CPage.objects.get(parent=parent)
        assert page.model == parent.model

        # Create child from multiple parent
        parent_model = factories.CModelFactory(type="multiple")
        parent = factories.CPageFactory(model=parent_model)
        data = {
            "title": "Hello",
            "template": "template.html",
            "parent": parent.pk,
            "lang": self.es_lang.pk,
        }
        self._test_post(ENDPOINTS["pages"], self.admin_user, 201, data, as_json=True)
        page = models.CPage.objects.get(parent=parent)
        assert page.model == parent.model

    def test_update_pages(self):
        """Try to update pages."""
        factories.CThemeFactory(templates=["template.html"])
        model = factories.CModelFactory(type="multiple")
        parent = factories.CPageFactory(description="desc")
        page = factories.CPageFactory(parent=parent)

        # Add model to a page
        data = {
            "title": "New title!",
            "type": "multiple",
            "model": model.pk,
            "parent": None,
            "lang": self.primary_lang.pk,
        }
        self._test_patch(
            "{}{}/".format(ENDPOINTS["pages"], parent.pk),
            self.admin_user,
            200,
            data,
            compare=False,
            as_json=True,
        )
        parent.refresh_from_db()
        assert parent.title == "New title!"
        assert parent.description == ""
        assert parent.slug == "new-title"
        assert parent.model == model

        page.refresh_from_db()
        assert page.model == parent.model

        # When setting a page with is_index, the old one is not the index anymore
        index_page = factories.CPageFactory(is_index=True)
        data = {
            "title": "New title!",
            "description": "Simple desc",
            "parent": None,
            "lang": self.primary_lang.pk,
            "is_index": True,
        }
        self._test_patch(
            "{}{}/".format(ENDPOINTS["pages"], parent.pk),
            self.admin_user,
            200,
            data,
            compare=False,
            as_json=True,
        )
        parent.refresh_from_db()
        index_page.refresh_from_db()

        parent.is_index is True
        index_page.is_index is False

    def test_destroy_pages(self):
        """Try to delete pages"""
        parent = factories.CPageFactory()
        parent2 = factories.CPageFactory()

        page = factories.CPageFactory(parent=parent)

        assert models.CPage.objects.count() == 4  # these 3 pages above + setupclass

        # A child page only can't be delete, it must be initiated with a parent
        self._test_delete("{}{}/".format(ENDPOINTS["pages"], page.pk), self.admin_user, 403)

        # Delete parent and child
        self._test_delete("{}{}/".format(ENDPOINTS["pages"], parent.pk), self.admin_user, 204)
        assert models.CPage.objects.count() == 2

        with self.assertRaises(models.CPage.DoesNotExist):
            parent.refresh_from_db()

        with self.assertRaises(models.CPage.DoesNotExist):
            page.refresh_from_db()

        # Delete a parent with no child
        self._test_delete(
            "{}{}/".format(ENDPOINTS["pages"], parent2.pk),
            self.admin_user,
            204,
        )
        assert models.CPage.objects.count() == 1

        with self.assertRaises(models.CPage.DoesNotExist):
            parent.refresh_from_db()
