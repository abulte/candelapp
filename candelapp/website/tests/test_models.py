"""Tests models."""
from django.core.exceptions import ValidationError
from django.test import TestCase
from django.utils.text import slugify

from candelapp.authentication.factories import UserFactory
from candelapp.international.models import Language
from candelapp.settings.models import Settings
from candelapp.website.models import CModelItem

from candelapp.website.factories import CModelFactory, CModelItemFactory, CPageFactory
from candelapp.website.tools import build_family_urls_with_lang_list


class CModelTestCase(TestCase):
    """Test CModel model."""

    def test_model(self):
        # By creating a model with a Factory
        model = CModelFactory(
            name="sup",
            verbose_name="Model Test",
        )

        # Name is not directly and is based on verbose_name
        assert model.name == "model_test"
        assert model.verbose_name == "Model Test"

        # Internal and verbose name can't be edited
        model.name = "edited_model"
        model.verbose_name = "Edited Model"
        model.type = "multiple"

        with self.assertRaisesMessage(
            ValidationError,
            repr("The model type can't be updated."),
        ):
            model.save(update_fields=["type"])

    def test_model_uniqueness(self):
        # When I have a model named 'Hello'
        CModelFactory(verbose_name="Hello")

        # It should raise an error to create a model with the same name
        with self.assertRaisesMessage(
            ValidationError,
            repr({"verbose_name": ["A model already exists with this name."]}),
        ):
            CModelFactory(verbose_name="Hello")

    def test_required_fields(self):
        # Title and description are required
        # Title is to be set as required (thus must be filled)

        error = repr(
            {
                "fields": [
                    "Title and description fields are required and are text types "
                    "in the fields list. The title must also always be set as required."
                ]
            }
        )
        fields_scenarios = [
            # No title
            [{"verbose_name": "Description", "required": False, "type": "text"}],
            # No description
            [{"verbose_name": "Title", "required": True, "type": "text"}],
            # Title and description but title is not required
            [
                {"verbose_name": "Title", "required": False, "type": "text"},
                {"verbose_name": "Description", "required": False, "type": "text"},
            ],
            # Not type text
            [
                {"verbose_name": "Title", "required": True, "type": "number"},
                {"verbose_name": "Description", "required": False, "type": "number"},
            ],
        ]
        for fields in fields_scenarios:
            with self.assertRaisesMessage(ValidationError, error):
                CModelFactory(fields=fields)

        # Ok
        fields = [
            {"verbose_name": "Title", "required": True, "type": "text"},
            {"verbose_name": "Description", "required": False, "type": "text"},
        ]
        CModelFactory(fields=fields)


class CModelItemTestCase(TestCase):
    """Test CModelItem model."""

    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        settings = Settings.objects.first()
        cls.primary_lang = settings.primary_language
        cls.es_lang = Language.objects.get(code="es")
        cls.fi_lang = Language.objects.get(code="fi")
        settings.secondary_languages.add(cls.es_lang, cls.fi_lang)

        cls.admin_user = UserFactory(
            email="alderson@allsafe.com",
            first_name="Eliott",
            last_name="Alderson",
        )
        cls.non_admin_user = UserFactory(
            email="moss@allsafe.com",
            first_name="Angela",
            last_name="Moss",
            is_staff=False,
        )

    def test_items_with_simple_model(self):
        """Modelitems with a simple model."""
        model = CModelFactory(verbose_name="A model name", type="simple")

        CModelItemFactory(model=model, parent=None)

        with self.assertRaisesMessage(
            ValidationError, "A simple model can only have one parent item."
        ):
            CModelItemFactory(model=model, parent=None)

    def test_modelitems_by_lang_and_models(self):
        """Test to get modelitems grouped by lang and models."""
        model1 = CModelFactory(verbose_name="A model name", type="multiple")
        model2 = CModelFactory(
            verbose_name="Another model name",
            type="multiple",
        )
        model3 = CModelFactory(verbose_name="A simple model", type="simple")

        cm1_parent = CModelItemFactory(model=model1, parent=None)
        cm2_parent = CModelItemFactory(model=model2, parent=None)
        CModelItemFactory(model=model3, parent=None)

        CModelItemFactory(parent=cm1_parent, model=model1, lang=self.fi_lang)
        CModelItemFactory(parent=cm2_parent, model=model2, lang=self.es_lang)

        grouped_modelitems = CModelItem.objects.group_modelitems_by_model()

        # There are 3 langs
        assert len(grouped_modelitems) == 3

        # There are 2 model items using model1 in english
        en_items = grouped_modelitems.pop("en")
        assert len(en_items) == 3
        # Ensure names are with an underscore for jinja2 in slugified model name
        assert list(en_items.keys()) == [
            "a_model_name",
            "another_model_name",
            "a_simple_model",
        ]

        # There is 1 model item using model1 in finnish
        fi_items = grouped_modelitems.pop("fi")
        assert len(fi_items) == 1

        # There is 1 model item using model1 in spanish
        # Model is multiple so will have a list
        es_items = grouped_modelitems.pop("es")
        assert len(es_items) == 1

        assert type(es_items["another_model_name"]) == list

        item = es_items["another_model_name"][0]
        assert item["model__type"] == "multiple"

        assert list(item.keys()) == [
            "lang__code",
            "model__name",
            "model__type",
            "created",
            "modified",
            "url",
            "data",
        ]

        # A simple model has a dict as there are no multiple items
        assert type(en_items["a_simple_model"]) == dict
        assert en_items["a_simple_model"]["model__type"] == "simple"

    def test_build_modelitem_urls(self):
        """Test url building on modelitems."""
        blogposts_model = CModelFactory(verbose_name="blogposts", type="multiple")
        products_model = CModelFactory(verbose_name="products", type="multiple")
        contact_model = CModelFactory(verbose_name="contact", type="simple")

        # Pages
        blogpost_page_detail_en = CPageFactory(
            title="blogposts",
            template="blogpost_detail.html",
            lang=self.primary_lang,
            model=blogposts_model,
        )
        blogpost_page_detail_fi = CPageFactory(
            title="articles de blog",
            template="blogpost_detail.html",
            lang=self.fi_lang,
            model=blogposts_model,
            parent=blogpost_page_detail_en,
        )

        # Parent with primary lang
        blogpost_cm1_en = CModelItemFactory(model=blogposts_model, lang=self.primary_lang)
        assert blogpost_cm1_en.url == "/{}/{}/".format(
            blogpost_page_detail_en.title, slugify(blogpost_cm1_en.data["title"])
        )

        # Child with fr lang
        blogpost_cm1_fi = CModelItemFactory(
            model=blogposts_model, lang=self.fi_lang, parent=blogpost_cm1_en
        )
        assert blogpost_cm1_fi.url == "/{}/{}/{}/".format(
            self.fi_lang.code,
            blogpost_page_detail_fi.slug,
            slugify(blogpost_cm1_fi.data["title"]),
        )

        # Parent with no child
        blogpost_cm2_en = CModelItemFactory(model=blogposts_model, lang=self.primary_lang)
        assert blogpost_cm2_en.url == "/{}/{}/".format(
            blogpost_page_detail_en.slug, slugify(blogpost_cm2_en.data["title"])
        )

        # Parent multiple with no page
        product_cmi_en = CModelItemFactory(
            model=products_model,
            lang=self.primary_lang,
        )
        assert product_cmi_en.url == ""

        product_cmi_fi = CModelItemFactory(
            model=products_model, lang=self.fi_lang, parent=product_cmi_en
        )
        assert product_cmi_fi.url == ""

        # Parent simple
        contact_cmi_en = CModelItemFactory(
            model=contact_model,
            lang=self.primary_lang,
        )
        assert contact_cmi_en.url == ""

        contact_cmi_fi = CModelItemFactory(
            model=contact_model, lang=self.fi_lang, parent=contact_cmi_en
        )
        assert contact_cmi_fi.url == ""

        # Test page update
        # Create another finnish blogpost
        # Page signal should update all modelitems linked to it

        blogpost_cm2_en = CModelItemFactory(model=blogposts_model, lang=self.primary_lang)
        blogpost_cm2_fi = CModelItemFactory(
            model=blogposts_model, lang=self.fi_lang, parent=blogpost_cm2_en
        )

        blogpost_page_detail_fi.title = "blogikirjoitukset"
        blogpost_page_detail_fi.save()

        blogpost_cm1_fi.refresh_from_db()
        blogpost_cm2_fi.refresh_from_db()

        assert blogpost_cm1_fi.url == "/{}/{}/{}/".format(
            self.fi_lang.code,
            "blogikirjoitukset",
            slugify(blogpost_cm1_fi.data["title"]),
        )
        assert "blogikirjoitukset" in blogpost_cm2_fi.url

        blogpost_cm2_en.refresh_from_db()
        assert "blogikirjoitukset" not in blogpost_cm2_en.url

    def test_get_cmi_family(self):
        """Get page family."""
        model1 = CModelFactory(type="multiple")
        model2 = CModelFactory(type="multiple")
        model3 = CModelFactory(type="simple")

        cm1_parent = CModelItemFactory(model=model1, parent=None)
        cm2_parent = CModelItemFactory(model=model2, parent=None)
        cm3_parent = CModelItemFactory(model=model3, parent=None)

        cm1 = CModelItemFactory(parent=cm1_parent, model=model1, lang=self.fi_lang)
        cm2 = CModelItemFactory(parent=cm1_parent, model=model1, lang=self.es_lang)
        cm3 = CModelItemFactory(parent=cm2_parent, model=model2, lang=self.es_lang)
        cm4 = CModelItemFactory(parent=cm3_parent, model=model3, lang=self.es_lang)

        # No modelitems linked to a page
        assert build_family_urls_with_lang_list(cm1.get_family()) == []

        cm1_family = cm1.get_family()

        # Family is a parent and the siblings
        assert cm1_parent in cm1_family
        assert cm1 in cm1_family
        assert cm2 in cm1_family

        cm3_family = cm3.get_family()
        assert cm2_parent in cm3_family
        assert cm3 in cm3_family

        cm4_family = cm4.get_family()
        assert cm3_parent in cm4_family

        # It works too from parent
        cm1_parent_family = cm1_parent.get_family()
        assert cm1_parent in cm1_parent_family
        assert cm1 in cm1_parent_family
        assert cm2 in cm1_parent_family

        # Create page for cm1_parent and cm1
        cm1_parent_page = CPageFactory(model=model1)
        CPageFactory(parent=cm1_parent_page, model=model1, lang=self.fi_lang)

        for cm in cm1_parent_family:
            cm.refresh_from_db()
        cm1_parent.refresh_from_db()
        cm1.refresh_from_db()
        cm2.refresh_from_db()

        # Test family list of dicts with lang code
        # Cm2 is not available as he got no link
        family_lang_and_url_list = build_family_urls_with_lang_list(cm1_parent_family)
        assert len(family_lang_and_url_list) == 2
        assert {cm1_parent.lang.code: cm1_parent.url} in family_lang_and_url_list
        assert {cm1.lang.code: cm1.url} in family_lang_and_url_list

        # Create page for cm2_parent and cm3
        cm2_parent_page = CPageFactory(model=model2)
        CPageFactory(parent=cm2_parent_page, model=model2, lang=self.es_lang)

        for cm in cm3_family:
            cm.refresh_from_db()
        cm2_parent.refresh_from_db()
        cm3.refresh_from_db()

        # Test family list of dicts with lang code
        family_lang_and_url_list = build_family_urls_with_lang_list(cm3_family)
        assert len(family_lang_and_url_list) == 2
        assert {cm2_parent.lang.code: cm2_parent.url} in family_lang_and_url_list
        assert {cm3.lang.code: cm3.url} in family_lang_and_url_list


class CPageTestCase(TestCase):
    """Test CPage model."""

    def test_page_model(self):
        """Test page with models"""
        simple_model = CModelFactory(type="simple")
        # A page can't be tied to a simple model
        with self.assertRaisesMessage(ValidationError, "A page cannot be tied to a simple model."):
            CPageFactory(model=simple_model)

        model = CModelFactory(type="multiple", verbose_name="Model Test")
        parent_page = CPageFactory(model=model)

        # A child page must have the parent's model
        with self.assertRaisesMessage(
            ValidationError, "A child must have the same parent's model."
        ):
            CPageFactory(parent=parent_page, model=None)

        # Ensure we can create a children with the same parent's model
        CPageFactory(parent=parent_page, model=model)

        # Another parent can't be created with the same model
        with self.assertRaisesMessage(
            ValidationError, "A model is unique for a parent and its children."
        ):
            CPageFactory(model=model)

    def test_get_page_family(self):
        """Test get page family."""
        parent_page1 = CPageFactory()
        page1 = CPageFactory(parent=parent_page1)
        page2 = CPageFactory(parent=parent_page1)

        parent_page2 = CPageFactory()
        page3 = CPageFactory(parent=parent_page2)

        # Get family of page1
        page1_family = page1.get_family()
        assert parent_page1 in page1_family
        assert page1 in page1_family
        assert page2 in page1_family

        # Get family of parent_page1
        parent_page1_family = parent_page1.get_family()
        assert parent_page1 in parent_page1_family
        assert page1 in parent_page1_family
        assert page2 in parent_page1_family

        # Get family of page1
        page3_family = page3.get_family()
        assert parent_page2 in page3_family
        assert page3 in page3_family

        # Test family list of dicts with lang code
        family_lang_and_url_list = build_family_urls_with_lang_list(page1_family)
        assert len(family_lang_and_url_list) == 3

        family_lang_and_url_list = build_family_urls_with_lang_list(page3_family)
        assert len(family_lang_and_url_list) == 2
