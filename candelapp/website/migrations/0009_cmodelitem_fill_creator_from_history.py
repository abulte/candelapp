from django.db import migrations


def set_creator_from_history(apps, schema_editor):
    CModelItem = apps.get_model("website", "CModelItem")
    HistoryLog = apps.get_model("history", "HistoryLog")
    for item in CModelItem.objects.all():
        log = HistoryLog.objects.filter(type="modelitem_add", new_state__id=item.pk)
        if len(log) and log[0].user and log[0].user.pk:
            item.creator = log[0].user
            item.save()


def reverse_func(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        ("website", "0008_cmodel_is_creator_visible"),
    ]

    operations = [migrations.RunPython(set_creator_from_history, reverse_func)]
