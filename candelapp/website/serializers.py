"""Website related serializers."""
from io import BytesIO
import zipfile
import tempfile

from django.core.files.uploadedfile import SimpleUploadedFile
from django.db import transaction
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _

import PIL
from rest_framework import serializers

from candelapp.website import constants
from candelapp.settings.models import Settings
from candelapp.tools import validators
from candelapp.website.mixins import DjangoExceptionMixin
from candelapp.website import models
from candelapp.website.datafield_serializers import (
    ChoiceField,
    ColorField,
    ExtrasField,
    ManyToOneField,
    OneToManyField,
    RelationTypeField,
    InternalNameField,
    MultipleChoiceField,
)


error_list = {
    "not_a_zip": _("The file is not a .zip"),
    "zip_too_big": _("The file is too big."),
    "missing_tpl": _("The directory containing the template files is missing."),
    "missing_base_tpl": (
        "One or more required templates are missing. (base.html, header.html,"
        "and footer.html are the minimum requirements)."
    ),
    "bad_field_format": _("A list is required for the model fields definition."),
    "bad_field_entry": ("The field definition is not correct."),
    "unique_name": _("A model already exists with this name."),
    "no_page_description": _("A description is required for this page type."),
    "model_in_use": _("The model set is already in use."),
    "simple_model_page": "A page cannot be tied to a simple model.",
    "simple_model_modelitem": ("Only one parent can be created with a simple model."),
    "not_existing_template": _("This template does not exist."),
    "not_allowed_field_names": _(
        f"Some field names are internal and reserved so cannot be used: {constants.RESERVED_FIELD_NAMES}",  # NOQA
    ),
    "forbidden_name_edition": _("The model name can't be updated."),
    "forbidden_type_edition": _("The model type can't be updated."),
    "unknown_fields": _("This field is not known."),
    "unique_name_fields": _("Two fields have the same or too similar name."),
    "title_and_description_fields_error": _(
        "Title and description fields are required and are text types in "
        "the fields list. The title must also always be set as required."
    ),
    "multiple_no_index": _("A page with a model set cannot be the index."),
    "wrong_parent_lang": _("The language of a parent must be the main language."),
    "wrong_child_lang": _(
        "The language of a child cannot be used by another child or be the main language."
    ),
    "lang_not_set": "This language does not exist or is not available.",
    "forbidden_child_fields": _("A child page cannot update the model nor the index status."),
}


SERIALIZER_TYPES = {
    "choice": ChoiceField(source=""),
    "color": ColorField(source=""),
    "file": serializers.CharField(source=""),
    "manytoone": ManyToOneField(source=""),
    "onetomany": OneToManyField(source=""),
    "image": serializers.CharField(source=""),
    "multiplechoice": MultipleChoiceField(source=""),
    "number": serializers.DecimalField(
        max_digits=None, decimal_places=None, coerce_to_string=False, source=""
    ),
    "text": serializers.CharField(source="", allow_blank=True, allow_null=False),
    "wysiwyg": serializers.CharField(source=""),
}


MANDATORY_TEMPLATES = ["header", "footer", "base"]


class CImageSerializer(serializers.ModelSerializer):
    """Serializer for CIMageViewsSet."""

    image = serializers.ImageField(validators=[validators.max_image_size])

    class Meta:
        model = models.CImage
        fields = ["pk", "image"]
        read_only_fields = ["pk"]

    def create(self, validated_data):
        """Convert and compress image to webp before creation."""
        image = validated_data["image"]
        # If image is a gif or svg. Don't optimize it
        if image.content_type not in ["image/gif", "image/svg"]:
            filename = "{}.webp".format(image.name.split(".")[0])
            im = PIL.Image.open(image)
            temp = BytesIO()
            im.save(temp, "WEBP", quality=70)
            validated_data["image"] = SimpleUploadedFile(name=filename, content=temp.getvalue())

        return super().create(validated_data)


class CFileSerializer(serializers.ModelSerializer):
    """Serializer for CFileViewsSet."""

    file = serializers.FileField(validators=[validators.max_file_size])

    class Meta:
        model = models.CFile
        fields = ["pk", "file"]
        read_only_fields = ["pk"]


class DataSerializer(serializers.Serializer):
    """Serializer for validating data according to model."""

    def __init__(self, *args, **kwargs):
        """Add serializer fields according to the model fields."""
        super(DataSerializer, self).__init__(*args, **kwargs)
        model = kwargs["context"].get("model")
        fields = model.fields
        # Dynamically create field for serializer to check on model ones
        for field in fields:
            field_key = field.get("internal_name")
            # The title should never be empty
            if field_key == "title":
                serializer_field = serializers.CharField(allow_blank=False, allow_null=False)
            else:
                serializer_field = SERIALIZER_TYPES.get(field.get("type"))
            serializer_field.required = field["required"]
            self.fields[field_key] = serializer_field

    def validate(self, data):
        """Raise error if unknown fields are in the data."""
        if hasattr(self, "initial_data"):
            unknown_keys = set(self.initial_data.keys()) - set(self.fields.keys())
            if unknown_keys:
                for key in unknown_keys:
                    raise serializers.ValidationError({key: error_list["unknown_fields"]})
        return data


class CModelCategorySerializer(serializers.ModelSerializer):
    """Serializer for CModel categories."""

    class Meta:
        model = models.CModelCategory
        fields = ["pk", "name"]
        read_only_fields = ["pk"]


class CModelItemSerializer(DjangoExceptionMixin, serializers.ModelSerializer):
    """Serializer for CModel items."""

    translated_status = serializers.SerializerMethodField()
    creator = serializers.SerializerMethodField()

    class Meta:
        model = models.CModelItem
        fields = [
            "pk",
            "created",
            "modified",
            "model",
            "status",
            "translated_status",
            "groups",
            "url",
            "data",
            "parent",
            "lang",
            "creator",
        ]
        read_only_fields = [
            "pk",
            "created",
            "modified",
            "model",
            "translated_status",
            "url",
            "creator",
        ]

    def get_creator(self, obj):
        """Expose basic informations about the creator, if model permits"""
        if not obj.model.is_creator_visible:
            return
        if obj.creator:
            return {
                "pk": obj.creator.pk,
                "first_name": obj.creator.first_name,
                "last_name": obj.creator.last_name,
            }

    def get_translated_status(self, obj):
        return obj.translated_status

    def get_primary_lang(self):
        """Get primary lang from settings."""
        self._primary_lang = Settings.objects.first().primary_language
        return self._primary_lang

    def validate_status(self, status):
        """
        Validate status when a user create or update a modelitem.

        Users should be able to update status of
        a modelitem.
        """
        # Request is in context if it comes from the modelviewset
        request = self.context.get("request")
        if not request or request.user.is_admin:
            return status

        # Allow only draft status for non admin users
        # if status != "draft":
        #     raise serializers.ValidationError(
        #         _("You do not have permission to use any other status than draft.")
        #     )

        return status

    def validate_groups(self, groups):
        """
        Validate groups when a user create a modelitem.

        When a user allowed to, he can create a modelitem.
        Only users with groups can assign groups to it.
        They won't be able to update them though.

        Ex: User in group1 and group2 creates a modelitem.
        He can assign group1 or group2 or both to modelitem.
        """
        # Request is in context if it comes from the modelviewset
        request = self.context.get("request")
        if not request or request.user.is_admin:
            return groups

        if self.instance and self.instance.groups != groups:
            raise serializers.ValidationError(_("A user can't update the ownership of an object."))

        user_groups_set = set(request.user.groups.values_list(flat=True))
        groups_set = set([group.pk for group in groups])

        # If not all specified groups in users groups
        if not groups_set.issubset(user_groups_set):
            raise serializers.ValidationError(_("User groups do not match the provided groups."))

        return groups

    def validate(self, data):
        """Validate the data and add model from url to validated one."""
        model_attribute = self.context["model_attribute"]
        lookup = {}
        try:
            int(model_attribute)
            lookup = {"pk": model_attribute}
        except ValueError:
            lookup = {"name": model_attribute}

        model_instance = models.CModel.objects.get(**lookup)

        self.get_primary_lang()

        parent = data.get("parent")
        lang = data.get("lang")

        # Lang and parent should not be updated directly
        if self.instance:
            parent = self.instance.parent
            lang = self.instance.lang
            data["parent"] = parent
            data["lang"] = lang

        if not parent:
            # Ensure a parent has the primary language set as lang
            if lang != self.get_primary_lang():
                raise serializers.ValidationError({"lang": error_list["wrong_parent_lang"]})
            # More than one parent can't be tied to a simple model
            if model_instance.type == "simple" and not self.instance:
                if models.CModelItem.objects.filter(parent=None, model=model_instance).count() > 0:
                    raise serializers.ValidationError(error_list["simple_model_modelitem"])
        else:
            # Ensure a child doesn't have the primary language set and
            # a secondary language already used
            instance_pk = None
            if self.instance:
                instance_pk = self.instance.pk
            children_langs_pk = (
                models.CModelItem.objects.filter(model=model_instance, parent=parent)
                .exclude(pk=instance_pk)
                .values_list("lang", flat=True)
            )
            if any(
                [
                    data["lang"] == parent.lang,
                    data["lang"].pk in children_langs_pk,
                ]
            ):
                raise serializers.ValidationError({"lang": error_list["wrong_child_lang"]})

        data_serializer = DataSerializer(
            data={**data.get("data", {})},
            context={"model": model_instance, "instance": self.instance, "lang": lang},
        )
        data_serializer.is_valid(raise_exception=True)
        data["model"] = model_instance

        return data

    def save(self, **kwargs):
        """
        Add 'drf_validated' attribute.
        The model save method won't validate
        """
        kwargs["drf_validated"] = True
        return super().save(**kwargs)


class CPageReadSerializer(serializers.ModelSerializer):
    """Read serializer for CPage."""

    model_name = serializers.SerializerMethodField()

    class Meta:
        model = models.CPage
        fields = [
            "pk",
            "created",
            "modified",
            "title",
            "description",
            "model",
            "model_name",
            "template",
            "is_index",
            "lang",
            "parent",
            "slug",
            "status",
        ]
        read_only_fields = fields

    def get_model_name(self, value) -> str:
        """Get model name from object."""
        model = getattr(value, "model")
        return model.name if model else None


class CPageCreateSerializer(serializers.ModelSerializer):
    """Create Serializer for CPage."""

    class Meta:
        model = models.CPage
        fields = [
            "pk",
            "title",
            "description",
            "model",
            "template",
            "is_index",
            "lang",
            "parent",
        ]

    def validate_parent(self, value):
        """
        Not really a validation.
        But if parent is specified, this is a child creation.
        model shoud not be required here.
        """
        if value:
            self.fields["model"].required = False
        return value

    def validate_template(self, value):
        """Validate that template exists."""
        theme = models.CTheme.objects.first()
        if not theme or value not in theme.templates:
            raise serializers.ValidationError(error_list["not_existing_template"])
        return value

    def validate_lang(self, value):
        """Ensure lang in payload is set in settings."""
        settings = Settings.objects.first()
        langs = []
        langs.append(settings.primary_language)
        langs.extend(list(settings.secondary_languages.all()))
        if value not in langs:
            raise serializers.ValidationError(error_list["lang_not_set"])

        return value

    def validate(self, data):
        """
        Ensure that a model is specified when page type is multiple.

        Sanitize fields that should not be filled anymore in case of type update.
        """
        if data.get("is_index") is True and data.get("model"):
            raise serializers.ValidationError({"is_index": error_list["multiple_no_index"]})

        parent = data.get("parent")
        if not parent:
            # Ensure a parent page can't use a model already in use
            model = data.get("model")
            if model:
                if models.CPage.objects.filter(model=model).exists():
                    raise serializers.ValidationError(
                        {"non_field_errors": error_list["model_in_use"]}
                    )
                # A page can't be tied to a simple model
                if model.type == "simple":
                    raise serializers.ValidationError(error_list["simple_model_page"])
            else:
                if not data.get("description"):
                    raise serializers.ValidationError(
                        {"description": error_list["no_page_description"]}
                    )
            # Ensure a parent page has the primary language set as lang
            default_lang = Settings.objects.first().primary_language
            if data.get("lang") != default_lang:
                raise serializers.ValidationError({"lang": error_list["wrong_parent_lang"]})
        else:
            # A child can't set is_index or model itself
            if any([data.get("is_index"), data.get("model")]):
                raise serializers.ValidationError(
                    {"non_field_errors": error_list["forbidden_child_fields"]}
                )

            # Ensure a child page doesn't have the primary language set and
            # a secondary language already used
            children_langs_pk = models.CPage.objects.filter(parent=parent).values_list(
                "lang", flat=True
            )
            if any(
                [
                    data["lang"] == parent.lang,
                    data["lang"].pk in children_langs_pk,
                ]
            ):
                raise serializers.ValidationError({"lang": error_list["wrong_child_lang"]})

        return data

    @transaction.atomic
    def create(self, validated_data):
        """Page object creation."""
        # Only one page can be the index
        if validated_data.get("is_index") is True:
            models.CPage.objects.update(is_index=False)

        # A child heritates the parent's model
        parent = validated_data.get("parent")
        if parent:
            validated_data["model"] = parent.model

        return super().create(validated_data)


class CPageUpdateSerializer(serializers.ModelSerializer):
    """Create Serializer for CPage."""

    class Meta:
        model = models.CPage
        fields = [
            "title",
            "description",
            "model",
            "template",
            "is_index",
        ]

    def validate_template(self, value):
        """Validate that template exists."""
        theme = models.CTheme.objects.first()
        if not theme or value not in theme.templates:
            raise serializers.ValidationError(error_list["not_existing_template"])
        return value

    def validate(self, data):
        """Page update validation."""
        # A child can't update is_index and model
        parent = getattr(self.instance, "parent")
        if parent:
            if any([data.get("is_index"), data.get("model")]):
                raise serializers.ValidationError(
                    {"non_field_errors": error_list["forbidden_child_fields"]}
                )
            if not parent.model and not data.get("description"):
                raise serializers.ValidationError(
                    {"description": error_list["no_page_description"]}
                )

        else:
            if data.get("model"):
                # Ensure a parent page can't use a model already in use
                # that is not its (and its children)
                model_already_in_use = (
                    models.CPage.objects.filter(model=data.get("model"))
                    .exclude(pk=self.instance.pk)
                    .exclude(parent=self.instance)
                    .exists()
                )
                if model_already_in_use:
                    raise serializers.ValidationError(
                        {"non_field_errors": error_list["model_in_use"]}
                    )
            else:
                if not data.get("description"):
                    raise serializers.ValidationError(
                        {"description": error_list["no_page_description"]}
                    )

        if data.get("model"):
            if data.get("is_index") is True or self.instance.is_index is True:
                raise serializers.ValidationError({"is_index": error_list["multiple_no_index"]})

        return super().validate(data)

    @transaction.atomic
    def update(self, instance, validated_data):
        """Update page instance."""
        # Only one page can be the index
        if validated_data.get("is_index") is True:
            models.CPage.objects.update(is_index=False)

        # A multiple page doesn't need a description anymore
        if validated_data.get("model"):
            validated_data["description"] = ""

        instance = super().update(instance, validated_data)

        # When the parent page is getting updated
        # Ensure all the children have the same model
        if instance.parent is None:
            children = models.CPage.objects.filter(parent=instance)
            if children:
                children.update(model=instance.model)

        return instance


class CFieldSerializer(serializers.Serializer):
    """Serializer for CModel fields."""

    verbose_name = serializers.CharField(max_length=100, min_length=2)
    internal_name = InternalNameField(max_length=100, min_length=2)
    required = serializers.BooleanField()
    extras = ExtrasField(required=False)
    type = serializers.ChoiceField(choices=constants.DATA_FIELD_TYPES)


class OneToManyFieldSerializer(CFieldSerializer):
    model = RelationTypeField()


class ManyToOneFieldSerializer(CFieldSerializer):
    model = RelationTypeField()


class ChoiceFieldSerializer(CFieldSerializer):
    choices = serializers.ListField()


class CModelSerializer(DjangoExceptionMixin, serializers.ModelSerializer):
    """Serializer for models."""

    category_name = serializers.SerializerMethodField()

    class Meta:
        model = models.CModel
        fields = [
            "pk",
            "name",
            "category",
            "category_name",
            "fields",
            "type",
            "verbose_name",
        ]
        read_only_fields = ["pk", "category_name", "name"]

    def get_category_name(self, obj) -> str:
        category = getattr(obj, "category", None)
        return category.name if category else None

    def validate_type(self, type):
        """Model type cannot be updated."""
        if self.instance:
            if self.instance.type != type:
                raise serializers.ValidationError(error_list["forbidden_type_edition"])
        return type

    def validate_fields_title_and_description(self, fields):
        """
        A model should always have a title and a description for internal purpose.

        As they can both be edited, ensure they still exist even when updating.
        The title field should always be required.
        """
        title = {}
        desc = {}
        for field in fields:
            if field.get("internal_name") == "title":
                title = field
            elif field.get("internal_name") == "description":
                desc = field

        if any(
            [
                not bool(title),
                not bool(desc),
                title.get("required") is not True,
                title.get("type") != "text",
                desc.get("type") != "text",
            ]
        ):
            raise serializers.ValidationError(error_list["title_and_description_fields_error"])
        return fields

    def validate_fields(self, fields):
        """
        Validate data fields against authorized ones.

        TODO: Improve to update per field
        Ideas: Set a UUID per field or use a model to store them
               Automatic migrations
        """
        if not isinstance(fields, list):
            raise serializers.ValidationError(error_list["bad_field_format"])

        for field in fields:
            internal_name = field.get("internal_name")
            verbose_name = field.get("verbose_name")
            field_type = field.get("type")

            # If user didn't set the internal name of the field
            if not internal_name:
                # Internal name is a slugified with underscores for jinja
                internal_name = slugify(verbose_name)
                field["internal_name"] = internal_name.replace("-", "_")

            # Choice and MultipleChoice fields need 'choices' values
            if field_type in ["choice", "multiplechoice"]:
                field_serializer = ChoiceFieldSerializer(data={**field})
            # Relations field needs a 'model' value
            elif field_type == "manytoone":
                field_serializer = ManyToOneFieldSerializer(data={**field})
            elif field_type == "onetomany":
                field_serializer = OneToManyFieldSerializer(data={**field})
            else:
                field_serializer = CFieldSerializer(data={**field})

            field_serializer.is_valid(raise_exception=True)

        # Validate uniqueness of internal names
        # (internal names are slugified verbose names)
        names = [field.get("internal_name") for field in fields]
        if len(set(names)) != len(names):
            raise serializers.ValidationError(error_list["unique_name_fields"])

        # Ensure no reserved field names are used
        if len(set(constants.RESERVED_FIELD_NAMES).difference(set(names))) != len(
            constants.RESERVED_FIELD_NAMES
        ):
            raise serializers.ValidationError(error_list["not_allowed_field_names"])

        self.validate_fields_title_and_description(fields)

        return fields

    def validate(self, data):
        """
        Slugify the verbose name to make the internal one.
        Ensure it is not already used.

        We are replacing dashes with underscores so internal names can work
        in jinja templates.
        """
        if not self.instance:
            slug_name = slugify(data["verbose_name"])
            name = slug_name.replace("-", "_")
            if models.CModel.objects.filter(name=name).exists():
                raise serializers.ValidationError({"verbose_name": error_list["unique_name"]})
            data["name"] = name
        return data

    def save(self, **kwargs):
        """
        Add 'drf_validated' attribute.
        The model save method won't validate
        """
        kwargs["drf_validated"] = True
        return super().save(**kwargs)


class CThemeSerializer(serializers.ModelSerializer):
    """CTheme serializer."""

    file = serializers.FileField(validators=[validators.max_file_size], write_only=True)

    class Meta:
        model = models.CTheme
        fields = ["pk", "file", "templates", "created", "modified"]
        read_only_fields = ["pk", "created", "modified", "templates"]

    def validate_file(self, value):
        """Validate that file sent is a zip and is legit."""
        if zipfile.is_zipfile(value) is False:
            raise serializers.ValidationError(error_list["not_a_zip"])

        try:
            archive = zipfile.ZipFile(value, "r")
        except zipfile.BadZipFile:
            raise serializers.ValidationError(error_list["not_a_zip"])
        except zipfile.LargeZipFile:
            raise serializers.ValidationError(error_list["zip_too_big"])

        self.list_files = archive.namelist()

        if "templates/" not in self.list_files:
            raise serializers.ValidationError(error_list["missing_tpl"])

        for tpl in MANDATORY_TEMPLATES:
            if "templates/{}.html".format(tpl) not in self.list_files:
                raise serializers.ValidationError(error_list["missing_base_tpl"])

        return value

    def validate(self, data):
        """Validate and pre populate data according to the file."""

        def filter_templates(file_path):
            """Filter all files to get only not mandatory and base templates."""
            full_path = "templates/"
            if full_path not in file_path:
                return False
            if file_path[-5:] != ".html":
                return False
            if "base_" in file_path:
                return False
            for tpl in MANDATORY_TEMPLATES:
                full_tpl_path = "{}{}.html".format(full_path, tpl)
                if full_tpl_path == file_path:
                    return False
            return True

        # Extract files so we can know what the templates are
        with zipfile.ZipFile(data["file"], "r") as archive:
            with tempfile.TemporaryDirectory() as tempdir:
                archive.extractall(tempdir)

        list_files_filtered = list(filter(filter_templates, self.list_files))
        list_templates_names = [tpl.split("/")[-1] for tpl in list_files_filtered]
        data["templates"] = list_templates_names

        return data

    def save(self, **kwargs):
        """
        Set instance to determine of we're going to a creation or an update.

        Only one theme can exist.
        """
        try:
            self.instance = models.CTheme.objects.first()
        except models.CTheme.DoesNotExist:
            self.instance = None

        return super().save(**kwargs)
