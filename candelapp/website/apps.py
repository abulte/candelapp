from django.apps import AppConfig


class WebsiteConfig(AppConfig):
    name = "candelapp.website"

    def ready(self):
        """Connect signals."""
        from . import signals  # NOQA
