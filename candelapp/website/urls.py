from django.urls import path

from rest_framework import routers

from candelapp.website import views


router = routers.SimpleRouter()
router.register(r"images", views.CImageViewSet)
router.register(r"files", views.CFileViewSet)
router.register(r"models", views.CModelViewSet)
router.register(r"modelcategories", views.CModelCategoryViewSet)
router.register(r"modelitems/(?P<model>[\w-]+)", views.CModelItemViewSet)
router.register(r"pages", views.CPageViewSet)
router.register(r"theme", views.CThemeViewSet)


urlpatterns = [
    path(r"generate/", views.GenerateAPIView.as_view()),
]

urlpatterns += router.urls
