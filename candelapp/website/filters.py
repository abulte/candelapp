"""Jinja2 filters."""
import random


def to_list(sequence):
    """Convert iterator over list."""
    new_list = list(sequence)
    return new_list


def shuffle(sequence):
    """Simply shuffle in random order a sequence."""
    new_list = list(sequence)
    random.shuffle(new_list)
    return new_list


def limit(sequence, n: int):
    """Limit a sequence to n elements with index 1."""
    return to_list(sequence)[:n]
