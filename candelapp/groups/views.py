from django.contrib.auth.models import Group

from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser

from candelapp.groups import serializers


class GroupViewset(viewsets.ModelViewSet):
    """A viewset to manage user groups."""

    model = Group
    queryset = Group.objects.all()
    serializer_class = serializers.GroupSerializer
    permission_classes = [IsAdminUser]
    http_method_names = ["get", "post", "patch", "delete"]
