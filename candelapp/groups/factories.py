"""Authentication related factories."""

import factory
from django.contrib.auth.models import Group


class GroupFactory(factory.django.DjangoModelFactory):
    """Factory for Group."""

    class Meta:
        model = Group

    name = factory.Faker("name")

    @factory.post_generation
    def users(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            for user in extracted:
                self.user_set.add(user)
