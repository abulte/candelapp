
from rest_framework import routers

from candelapp.groups import views


router = routers.SimpleRouter()
router.register(r"", views.GroupViewset)

urlpatterns = router.urls
