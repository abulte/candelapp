from django.contrib.auth.models import Group

from rest_framework import serializers


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = [
            "pk",
            "name",
            "user_set",
        ]
        read_only_fields = ["pk", "user_set"]


class UserGroupsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = [
            "pk",
            "name",
        ]
        read_only_fields = ["pk", "name"]
