"""Custom validators."""
from django.conf import settings
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


def max_file_size(value):
    """File size validator."""
    if value.size > settings.MAX_FILE_UPLOAD_SIZE:
        error_msg = _("The file is too big.")
        raise ValidationError(error_msg)


def max_image_size(value):
    """Image size validator."""
    if value.size > settings.MAX_IMAGE_UPLOAD_SIZE:
        error_msg = _("The picture is too big.")
        raise ValidationError(error_msg)
