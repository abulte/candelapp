"""Network related tools."""


def get_ip_address_from_request(request):
    """
    Get the remote ip address from the request.

    Hide the two last octets (16 bits) to comply with the GPDR.

    TODO: Handle ipv6 addresses.
    """
    # Get the client's IP address from the request's META dictionary
    ipaddr = request.META.get("REMOTE_ADDR", "")

    # Check if the client's IP address is forwarded by a proxy
    if "HTTP_X_FORWARDED_FOR" in request.META:
        forwarded_ips = request.META["HTTP_X_FORWARDED_FOR"].split(",")
        # The client's IP address is the first one in the list of forwarded IPs
        ipaddr = forwarded_ips[0].strip()
    if ipaddr:
        octets = ipaddr.split(".")
        ipaddr = f"{octets[0]}.{octets[1]}.x.x"
    return ipaddr
