from django.utils.translation import gettext as _

from django.contrib.auth.models import Group

from rest_framework import serializers

from candelapp.website.models import CModel


class GroupListSerializer(serializers.ListField):
    def to_representation(self, data):
        return data

    def to_internal_value(self, data):
        if not isinstance(data, list):
            raise serializers.ValidationError(
                _(
                    "Incorrect type. Expected a list of primary keys "
                    f"but got {type(data).__name__}"
                )
            )
        for item in data:
            if not isinstance(item, int):
                raise serializers.ValidationError(
                    _(
                        "Incorrect type. Expected a list of primary "
                        f"keys but got {type(item).__name__}"
                    )
                )
        if len(data) != Group.objects.filter(pk__in=data).count():
            raise serializers.ValidationError("One or more groups don't exist.")
        return data


class PermissionSerializer(serializers.Serializer):
    groups = GroupListSerializer(child=serializers.IntegerField(), default=[])
    allow_everyone = serializers.BooleanField(default=False)
    allow_creator = serializers.BooleanField(default=False)
    groups_own_objects = serializers.BooleanField(default=False)


class PermissionsSerializer(serializers.ModelSerializer):
    can_add = PermissionSerializer()
    can_delete = PermissionSerializer()
    can_edit = PermissionSerializer()
    can_view = PermissionSerializer()

    class Meta:
        model = CModel
        fields = [
            "pk",
            "name",
            "verbose_name",
            "can_add",
            "can_delete",
            "can_edit",
            "can_view",
            "is_public",
            "is_creator_visible",
        ]
        read_only_fields = ["pk", "name", "verbose_name"]
