"""Settings views."""
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAdminUser

from candelapp.website.models import CModel
from candelapp.permissions import serializers


class PermissionsViewSet(ModelViewSet):
    """A viewset for editing permissions."""

    http_method_names = ["get", "patch"]
    queryset = CModel.objects.all()
    permission_classes = [IsAdminUser]
    serializer_class = serializers.PermissionsSerializer
