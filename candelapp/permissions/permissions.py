from rest_framework.permissions import BasePermission

from candelapp.website.models import CModel, CModelItem


class ModelPermissions(BasePermission):
    """
    Custom permission to check if a user can view the model.

    Wether the user is anonymous or registered, models have the "is_public"
    attribute that allows every users to be able to view a model.

    Only the detail view is allowed though.
    """

    def has_permission(self, request, view):
        if view.queryset.model != CModel:
            raise Exception("ModelPermissions must be used with the CModel.")
        if request.method != "GET":
            return False
        elif view.kwargs == {}:
            return False
        return True

    def has_object_permission(self, request, view, obj):
        return obj.is_public


class ModelItemPermissions(BasePermission):
    """
    Custom permission class that checks if the user's groups have the necessary permissions.
    """

    def has_permission(self, request, view):
        """
        Check GET list and POST request.
        GET detail is handled by 'has_object_permissions'.

        Permission allows 4 users type to create or list model items:
        - all users with 'allow_everyone' set to True
        - authenticated users with 'groups_own_objects' set to True
        - authenticated users with 'allow_creator' set to True
        - authenticated users who are part of a group authorized by the permission
        """
        if view.queryset.model != CModelItem:
            raise Exception("ModelItemPermissions must be used with the CModelItem.")

        lookup = None
        model_kwargs = view.kwargs.get("model")
        try:
            int(model_kwargs)
            lookup = {"pk": model_kwargs}
        except ValueError:
            lookup = {"name": model_kwargs}
        except TypeError:
            return False

        if request.method == "GET" and not view.kwargs.get("pk", None) or request.method == "POST":
            model = CModel.objects.get(**lookup)
            permissions = model.can_view if request.method == "GET" else model.can_add

            if request.user.is_anonymous:
                return permissions.get("allow_everyone", False)
            else:
                if (
                    permissions.get("allow_everyone", False)
                    or permissions.get("groups_own_objects", False)
                    or permissions.get("allow_creator", False)
                ):
                    return True

                user_groups = request.user.groups.values_list(flat=True)
                perm_groups = permissions.get("groups", [])
                return any(elem in perm_groups for elem in user_groups)
            return False

        # Return True here so the check object permissions can be processed
        # if any of the cases are met
        return True

    def has_object_permission(self, request, view, obj):
        """
        This method handles can_view, can_edit, can_delete.

        It gets the permissions according to the request type.
        """
        permissions = self.get_permissions(request, obj)

        if request.user.is_anonymous:
            return permissions.get("allow_everyone", False)
        elif request.user.is_authenticated:
            if permissions.get("allow_everyone", False) or permissions.get("allow_creator", False):
                return True
            user_groups = request.user.groups.values_list(flat=True)
            perm_groups = permissions.get("groups", [])
            obj_groups = list(obj.groups.values_list(flat=True))
            groups_own_objects = permissions.get("groups_own_objects", False)

            if groups_own_objects and any(elem in obj_groups for elem in user_groups):
                return True
            elif any(elem in perm_groups for elem in user_groups):
                return True

        return False

    def get_permissions(self, request, obj):
        permissions = {}
        if request.method in ["PATCH", "PUT"]:
            permissions = obj.model.can_edit
        elif request.method == "GET":
            permissions = obj.model.can_view
        elif request.method == "DELETE":
            permissions = obj.model.can_delete
        return permissions
