from django.db import models
from django.utils.translation import gettext_lazy as _

from django.contrib.auth import get_user_model


User = get_user_model()


ACTION_TYPES = [
    ("user_add", _("User creation")),
    ("user_change", _("User modification")),
    ("user_connect", _("User connection")),
    ("user_delete", _("User deletion")),
    ("user_toggle_admin", _("User toggle admin")),
    ("model_add", _("Model creation")),
    ("model_change", _("Model modification")),
    ("model_delete", _("Model deletion")),
    ("modelitem_add", _("Modelitem creation")),
    ("modelitem_change", _("Modelitem modification")),
    ("modelitem_delete", _("Modelitem deletion")),
]


ACTION_SENTENCES = {
    "user_add": _("User {email} has been created by {action_user}."),
    "user_change": _("User {email} has been modified by {action_user}."),
    "user_connect": _("User {email} has logged in with the ip address {ip}."),
    "user_delete": _("User {email} has been deleted by {action_user}."),
    "user_to_admin": _("User {email} has been promoted admin by {action_user}."),
    "admin_to_user": _("User {email} has been demoted from admin by {action_user}."),
    "model_add": _("Model {model_name} has been created by {action_user}."),
    "model_change": _("Model {model_name} has been modified by {action_user}."),
    "model_delete": _("Model {model_name} has been deleted by {action_user}."),
    "modelitem_add": _("Item {modelitem_name} has been created by {action_user}."),
    "modelitem_change": _("Item {modelitem_name} has been modified by {action_user}."),
    "modelitem_delete": _("Item {modelitem_name} has been deleted by {action_user}."),
}


class HistoryLog(models.Model):
    type = models.CharField(max_length=50, choices=ACTION_TYPES)
    params = models.JSONField(blank=True, null=True, default=dict)
    user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    prev_state = models.JSONField(blank=True, null=True, default=dict)
    new_state = models.JSONField(blank=True, null=True, default=dict)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.type

    @property
    def translated_action(self):
        sentence = ACTION_SENTENCES.get(self.type)
        if not self.params.get("action_user"):
            if self.params.get("is_anon", True):
                self.params["action_user"] = _("an anonymous user")
            else:
                self.params["action_user"] = _("the system")
        elif self.type == "user_toggle_admin":
            if self.params["is_admin"]:
                sentence = ACTION_SENTENCES.get("user_to_admin")
            else:
                sentence = ACTION_SENTENCES.get("admin_to_user")
        try:
            return sentence.format(**self.params, user=self.user)
        except AttributeError:
            return _("An error occured while translating the action.")
