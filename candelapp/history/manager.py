from datetime import date, datetime
import json
import logging

from django.db.models.base import ModelBase, ModelState

from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser

from rest_framework.serializers import SerializerMetaclass

from candelapp.history.models import HistoryLog
from candelapp.website.models import CModel, CModelItem
from candelapp.tools.network import get_ip_address_from_request


User = get_user_model()
SENSITIVE_FIELDS = ("password",)

logger = logging.getLogger("candelapp.history")


default_params = {
    "action_user": None,
    "is_anon": False,
    "ip": None,
    "email": None,
    "model_name": None,
    "modelitem_name": None,
    "is_admin": False,
}


class LogManager:
    def __str__(self):
        return self.log.translated_action

    def add_log_entry(self, type: str, instance, user: User = None, **kwargs):
        self.log = HistoryLog(type=type)
        # AnonymousUser from request should be considered as None
        self.log.params = default_params.copy()
        if isinstance(user, AnonymousUser):
            user = None
            self.log.params["is_anon"] = True
        if user:
            self.log.params["action_user"] = user.email
        self.log.user = user
        set_action_params = getattr(self, type)
        set_action_params(instance, **kwargs)
        if user:
            self.log.params["action_user"] = user.email
        self.perform_save(**kwargs)
        return self.log

    def user_add(self, instance: User, **kwargs) -> None:
        self.log.params = {"email": instance.email}

    def user_change(self, instance: User, **kwargs) -> None:
        self.log.params = {"email": instance.email}

    def user_delete(self, instance: User, **kwargs) -> None:
        self.log.params = {"email": instance.email}

    def user_toggle_admin(self, instance: User, **kwargs) -> None:
        self.log.params = {"email": instance.email, "is_admin": instance.is_admin}

    def user_connect(self, instance: User, **kwargs) -> None:
        request = kwargs["request"]
        ip = get_ip_address_from_request(request)
        self.log.params = {"email": instance.email, "ip": ip}

    def model_add(self, instance: CModel, **kwargs) -> None:
        self.log.params = {"model_name": instance.verbose_name}

    def model_change(self, instance: CModel, **kwargs) -> None:
        self.log.params = {"model_name": instance.verbose_name}

    def model_delete(self, instance: CModel, **kwargs) -> None:
        self.log.params = {"model_name": instance.verbose_name}

    def modelitem_add(self, instance: CModelItem, **kwargs) -> None:
        self.log.params = {"modelitem_name": instance.data["title"]}

    def modelitem_change(self, instance: CModelItem, **kwargs) -> None:
        self.log.params = {"modelitem_name": instance.data["title"]}

    def modelitem_delete(self, instance: CModelItem, **kwargs) -> None:
        self.log.params = {"modelitem_name": instance.data["title"]}

    def clean(self, state=None):
        """
        Clean the dict from instance to be json serializable and safe.

        - Remove _state, SerializerMetaClass and ModelBase attributes from instance
        - Remove callable functions
        - Hide sensitive data like passwords
        - Convert datetime to isoformat
        """
        if not state:
            return state

        for field in SENSITIVE_FIELDS:
            if state.get(field) is not None:
                state[field] = "******"
        for field in set(state):
            if type(state[field]) == SerializerMetaclass:
                state.pop(field)
            elif type(state[field]) in (date, datetime):
                state[field] = state[field].isoformat()
            elif isinstance(state[field], (ModelBase, ModelState, SerializerMetaclass)):
                state.pop(field)
            elif callable(state[field]):
                state.pop(field)

        # Ensure the state if json serializable
        try:
            json.dumps(state)
        except TypeError as e:
            logger.error(f"JSON not serializable: {str(e)}")
            return None

        return state

    def perform_save(self, **kwargs) -> HistoryLog:
        self.log.prev_state = self.clean(kwargs.get("prev_state"))
        self.log.new_state = self.clean(kwargs.get("new_state"))
        self.log.save()
        return self.log
