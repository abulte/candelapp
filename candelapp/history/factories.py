from datetime import datetime
import factory

from candelapp.authentication.factories import UserFactory
from candelapp.history.models import HistoryLog


class HistoryLogFactory(factory.django.DjangoModelFactory):
    """Factory for HistoryLog."""

    class Meta:
        model = HistoryLog

    params = {
        "email": "hello@how.low",
        "ip": "127.0.x.x",
        "model_name": "Hey",
        "modelitem_name": "Ho",
    }
    user = factory.SubFactory(UserFactory)
    type = "user_add"
    timestamp = datetime.now()
    prev_state = {}
    new_state = {}
