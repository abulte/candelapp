from django_filters.rest_framework import DjangoFilterBackend
from drf_spectacular.utils import extend_schema, OpenApiParameter
from drf_spectacular.types import OpenApiTypes
from rest_framework import filters, viewsets
from rest_framework.permissions import IsAdminUser

from candelapp.history.serializers import HistoryLogModelSerializer
from candelapp.history.models import HistoryLog


@extend_schema(
    parameters=[
        OpenApiParameter("subject_id", OpenApiTypes.INT, OpenApiParameter.QUERY),
    ],
)
class HistoryViewSet(viewsets.ModelViewSet):
    """
    History log list view.

    You can filter the queryset by searching users email or logs type.

    Ex:

    ?type=user_add,modelitem_change
    ?type=modelitem_add

    ?user__email=user@candelapp.com

    You can also filter by the primary key of the object on which an action
    has taken place (the subject);

    ?subject_id=42
    """

    queryset = HistoryLog.objects.order_by("-timestamp")
    serializer_class = HistoryLogModelSerializer
    permission_classes = [IsAdminUser]
    filter_backends = [filters.OrderingFilter, DjangoFilterBackend]
    filterset_fields = {
        "type": ["in", "exact"],
        "user__email": ["exact"]
    }
    http_method_names = ["get"]

    def get_queryset(self):
        queryset = HistoryLog.objects
        subject_id = self.request.query_params.get("subject_id")
        if subject_id is not None:
            queryset = queryset.filter(new_state__id=int(subject_id))
        return queryset.order_by("-timestamp")
