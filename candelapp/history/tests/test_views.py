from candelapp.authentication.factories import AdminFactory, UserFactory
from candelapp.history.factories import HistoryLogFactory
from candelapp.tools import test_utils


ENDPOINTS = {
    "history": "/api/v1/history/",
}


class HistoryViewTestCase(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        cls.admin_user = AdminFactory()
        cls.non_admin_user = UserFactory()
        cls.log = HistoryLogFactory(user=cls.admin_user, type="user_add")

    def test_get(self):
        # Not logged in
        self._test_get(ENDPOINTS["history"], None, 401)

        # Non admin user
        self._test_get(ENDPOINTS["history"], self.non_admin_user, 403)

        # Test list
        response = self._test_get(ENDPOINTS["history"], self.admin_user, 200)
        assert len(response.json()) == 1

    def test_get_by_id(self):
        url = f"{ENDPOINTS['history']}{self.log.pk}/"
        response = self._test_get(url, self.admin_user, 200)
        assert response.json()["pk"] == self.log.pk

    def test_get_by_subject_id(self):
        HistoryLogFactory(user=self.admin_user, type="model_change", new_state={"id": 42})
        url = f"{ENDPOINTS['history']}?subject_id=42"
        response = self._test_get(url, self.admin_user, 200)
        data = response.json()
        assert len(data) == 1
        assert data[0]["new_state"]["id"] == 42

    def test_get_logs_by_type(self):
        HistoryLogFactory(user=self.admin_user, type="model_change")

        # By filtering with user_add
        response = self._test_get(ENDPOINTS["history"] + "?type=user_add", self.admin_user, 200)
        assert len(response.json()) == 1
        response.json()[0]["type"] == "user_add"

        # By filtering with model_change
        response = self._test_get(
            ENDPOINTS["history"] + "?type=model_change", self.admin_user, 200
        )
        assert len(response.json()) == 1
        response.json()[0]["type"] == "model_change"

        # By filtering with both
        response = self._test_get(
            ENDPOINTS["history"] + "?type__in=model_change,user_add", self.admin_user, 200
        )
        assert len(response.json()) == 2

    def test_get_logs_by_user(self):
        HistoryLogFactory(user=self.non_admin_user, type="user_add")

        # By filtering with non admin user
        response = self._test_get(
            ENDPOINTS["history"] + f"?user__email={self.non_admin_user.email}", self.admin_user, 200
        )
        assert len(response.json()) == 1
        assert response.json()[0]["user"] == self.non_admin_user.pk

        # By filtering with admin user
        response = self._test_get(
            ENDPOINTS["history"] + f"?user__email={self.admin_user.email}", self.admin_user, 200
        )
        assert len(response.json()) == 1
        assert response.json()[0]["user"] == self.admin_user.pk
