import pytz

from django.conf import settings

from candelapp.authentication.factories import UserFactory
from candelapp.history.factories import HistoryLogFactory
from candelapp.history import serializers
from candelapp.tools import test_utils


TZ = pytz.timezone(settings.TIME_ZONE)


class HistoryLogSerializerTest(test_utils.TestAPIMixin, test_utils.APITestCaseWithData):
    @classmethod
    def setUpTestData(cls):
        """Set up users."""
        super().setUpTestData()
        cls.user = UserFactory()
        cls.log = HistoryLogFactory(user=cls.user)

    def test_serializer(self):
        serializer = serializers.HistoryLogModelSerializer(self.log)
        assert serializer.data == {
            "action": self.log.translated_action,
            "new_state": self.log.new_state,
            "pk": self.log.pk,
            "prev_state": self.log.prev_state,
            "timestamp": self.log.timestamp.astimezone(TZ).isoformat(),
            "type": self.log.type,
            "user": self.log.user.pk,
        }
