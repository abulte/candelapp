from rest_framework import serializers

from candelapp.history.models import HistoryLog


class HistoryLogModelSerializer(serializers.ModelSerializer):

    action = serializers.SerializerMethodField()

    class Meta:
        model = HistoryLog
        fields = ("pk", "timestamp", "action", "prev_state", "new_state", "user", "type")

    def get_action(self, obj):
        return obj.translated_action
