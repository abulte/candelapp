from rest_framework import routers

from candelapp.history import views

router = routers.SimpleRouter()
router.register(r"", views.HistoryViewSet)

urlpatterns = router.urls
