import getEnv from "src/utils/env";

// Get patterns from env var that are set by docker.
// Get tenant from backoffice url and build Api url from Api pattern.
export const getApiUrl = () => {
  const backofficeUrlPattern = getEnv("BACKOFFICE_URL_PATTERN");
  const apiUrlPattern = getEnv("API_URL_PATTERN");
  let apiUrl = null;
  let tenantPositionInUrl = null;
  backofficeUrlPattern.split(".").map((item, index) => {
    if (item === "{tenant}") {
      tenantPositionInUrl = index;
    }
  });
  const tenant = window.location.hostname.split(".")[tenantPositionInUrl];
  const apiUrlWithTenant = apiUrlPattern.replace("{tenant}", tenant);
  apiUrl = `${apiUrlWithTenant}/api/v1/`;
  return apiUrl;
};
