import { store } from "src/store";
import { colors } from "quasar";

export function getSecondaryLanguagePkFromCode(code) {
  const languages = store.getters["settings/getLanguages"].secondary_languages;
  let pk = null;
  languages.find(function(language, index) {
    if (language.code === code) {
      pk = language.pk;
    }
  });
  return pk;
}

// Based on https://coderwall.com/p/z8uxzw/javascript-color-blender
const hex_to_rbg = hex => {
  /*
  #xxx or #xxxxxx to rgb(x, x, x)
  */
  let rgb;
  if (hex.length == 4) {
    rgb = hex[1] + hex[1] + hex[2] + hex[2] + hex[3] + hex[3];
  } else {
    rgb = hex.substring(1);
  }
  return [
    parseInt(rgb[0] + rgb[1], 16),
    parseInt(rgb[2] + rgb[3], 16),
    parseInt(rgb[4] + rgb[5], 16)
  ];
};

const rgb_to_hex = rgb => {
  /*
  rgb(x, x, x) to #xxxxxx
  */
  return "#" + int_to_hex(rgb[0]) + int_to_hex(rgb[1]) + int_to_hex(rgb[2]);
};

const int_to_hex = integer => {
  let hex = Math.round(integer).toString(16);
  if (hex.length == 1) hex = "0" + hex;
  return hex;
};

export function blend_colors(hex1, hex2) {
  const blend_percentage = 0.7;
  const rgb1 = hex_to_rbg(hex1);
  const rgb2 = hex_to_rbg(hex2);
  const rgb3 = [
    (1 - blend_percentage) * rgb1[0] + blend_percentage * rgb2[0],
    (1 - blend_percentage) * rgb1[1] + blend_percentage * rgb2[1],
    (1 - blend_percentage) * rgb1[2] + blend_percentage * rgb2[2]
  ];
  return rgb_to_hex(rgb3);
}

export function updateBrand(candelappColors) {
  for (const key in candelappColors) {
    colors.setBrand(key, candelappColors[key]);
  }
}
