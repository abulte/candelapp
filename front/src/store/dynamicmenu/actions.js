import apiInstance from "src/api/base";

export async function updateDynamicMenu(state, models) {
  if (!models) {
    try {
      const response = await apiInstance.get("/website/models/");
      models = response.data;
    } catch (error) {
      throw error;
    }
  }
  state.commit("SET_DYNAMIC_MENU", models);
}
