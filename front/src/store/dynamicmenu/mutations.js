import _ from "lodash";

const MODEL_ORDER = {
  Catalogue: [
    "Jeux de données",
    "Organisations",
    "Réutilisations",
    "Outils",
    "Collections",
    "Bouquets de données",
    "Urls Inactives",
    "Commentaires",
    "Like users",
    "Charte de réutilisation"
  ],
  "Ressources juridiques": [
    "Ficher ressourcerie juridique",
    "Parcours OJAD",
    "Questions",
    "Conditions",
    "Réponses",
    "Séctions Résultat"
  ],
  "Projets données": ["Appel à projets", "Expé URBA SanTé", "Challenge GD4H"],
  Communauté: ["L'info de la semaine", "Actualités", "Indicateurs"],
  "Liste de valeurs contrôlées": []
};

const sortByList = (e1, e2, list, key = "category_name") => {
  const i1 = list.findIndex(e => e === e1[key]);
  const i2 = list.findIndex(e => e === e2[key]);
  if (i1 === -1) return 1;
  if (i2 === -1) return -1;
  return i1 - i2;
};

const sortDataModelsByCat = (cat, dataModels) => {
  switch (cat) {
    case "Catalogue":
    case "Ressources juridiques":
    case "Projets données":
    case "Communauté":
      return dataModels.toSorted((e1, e2) =>
        sortByList(e1, e2, MODEL_ORDER[cat], "verbose_name")
      );
    case "Liste de valeurs contrôlées":
      return dataModels.toSorted((a, b) =>
        a.verbose_name.localeCompare(b.verbose_name)
      );
  }
};

export async function SET_DYNAMIC_MENU(state, data) {
  const obj = _.groupBy(data, "category_name");
  state.menu = Object.keys(obj)
    .map(key => ({
      categoryName: key,
      categoryValue: sortDataModelsByCat(key, obj[key])
    }))
    .toSorted((e1, e2) =>
      sortByList(e1, e2, Object.keys(MODEL_ORDER), "categoryName")
    );
}
