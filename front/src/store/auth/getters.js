export function user(state) {
  return state.user;
}

export function isAuthenticated(state) {
  return !state.accessToken == "" || !state.accessToken == undefined;
}

export function accessToken(state) {
  return state.accessToken;
}
