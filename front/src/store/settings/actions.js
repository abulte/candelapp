import { LocalStorage } from "quasar";

import apiInstance from "src/api/base";

export function initSettings(state) {
  let settings = LocalStorage.getItem("settings");
  let languages = LocalStorage.getItem("languages");
  if (settings !== "" || settings !== undefined) {
    state.commit("SET_SETTINGS", JSON.parse(settings));
  }
  if (languages !== "" || languages !== undefined) {
    state.commit("SET_LANGUAGES", JSON.parse(languages));
  }
}

export async function getSettings(state) {
  let response;
  try {
    response = await apiInstance.get("/settings/settings/");
    state.commit("SET_SETTINGS", response.data);
  } catch (error) {
    throw error;
  }
  return response;
}

export async function getInterfaceSettings(state) {
  let response;
  try {
    response = await apiInstance.get("/settings/interface/");
    state.commit("SET_INTERFACE_SETTINGS", response.data);
  } catch (error) {
    throw error;
  }

  return response;
}

export async function updateSettings(state, data) {
  let response;
  try {
    response = await apiInstance.patch("/settings/settings/", data);
    state.commit("SET_SETTINGS", response.data);
  } catch (error) {
    throw error;
  }
  return response;
}

export async function getLanguages(state) {
  let response;
  try {
    response = await apiInstance.get("/settings/languages/");
    state.commit("SET_LANGUAGES", response.data);
  } catch (error) {
    throw error;
  }
  return response;
}
