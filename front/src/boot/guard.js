export default ({ router, store, Vue }) => {
  router.beforeEach((to, from, next) => {
    const record = to.matched.find((record) => record.meta.requiresAuth);
    // If a route requires to be authenticated
    if (record) {
      // We set again the token from LocalStorage to state
      // Even if the token is wrong, the first call to the API
      // will return a 401 error. This part is then processed
      // by base.js
      store.dispatch("auth/initAuth");
      // No access token in state
      if (!store.getters["auth/isAuthenticated"]) {
        // Clean all tokens and redirect to login
        store.dispatch("auth/logoutUser");
        next({ name: "login" });
      } else next();
    } else next();
  });
};
