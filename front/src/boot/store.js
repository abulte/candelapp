// Mainly used to re populate important data in store if user refresh the app
export default ({ router, store, Vue }) => {
  router.beforeEach((to, from, next) => {
    // if user is connected
    if (store.getters["auth/isAuthenticated"]) {
      // Ensure settings are initiated
      store.dispatch("settings/initSettings");
    }
    next();
  });
};
