import VueBreadcrumbs from "vue-2-breadcrumbs";
import { QBreadcrumbs, QBreadcrumbsEl } from "quasar";

import apiInstance from "src/api/base.js";

export default async ({ Vue }) => {
  Vue.prototype.$apiInstance = apiInstance;
  Vue.use(VueBreadcrumbs);
};
