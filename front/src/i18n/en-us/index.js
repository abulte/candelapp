// Translations are stored by components / pages

export default {
  misc: {
    others: "Others",
    required: "Required",
    example: "Example",
    minimumCharacters: "minimum characters",
    maximumCharacters: "maximum characters"
  },
  welcome: {
    welcome: "Welcome"
  },
  login: {
    email: "Email",
    password: "Password",
    login: "Log in",
    notAuthorized: "You are not authorized to log in here."
  },
  404: {
    message: "Oh, there's nothing to see here...",
    return: "Return to the modelling page"
  },
  logs: {
    title: "Logs",
    entry: "Entry",
    date: "Date"
  },
  pages: {
    title: "Pages",
    homepage: "Home page",
    create: "Create a page",
    edit: "Edit",
    delete: "Delete",
    promptDeleteTitle: "Do you really want to delete the page?",
    promptDeleteMessageNoModel:
      "You are about to delete this page. This page in all its languages will no longer be available when the site is next updated. Do you agree?",
    promptDeleteMessageWithModel:
      "You are about to delete this page in all its languages. This page is also linked to a data model. All the objects in this model will no longer have their page generated. Do you agree?",
    entryTitle: "Page",
    entryDataModel: "Associated data model",
    deleteConfirm: "The page has been deleted."
  },
  theme: {
    send: "Send",
    lastUpdate: "Last update",
    templateList: "Templates list",
    importTheme: "Import a theme",
    fileSizeHint: ".zip file smaller than 10 megabytes",
    fileSizeError: "Le fichier dépasse la taille autorisée",
    confirmationImported: "Le thème a bien été importé."
  },
  users: {
    title: "Users",
    create: "Create a user",
    admin: "Administrator",
    user: "User",
    producer: "Producer",
    reuser: "Reuser",
    edit: "Edit",
    delete: "Delete",
    revokeAdmin: "Revoke administrator rights",
    grantAdmin: "Grant administrator rights",
    promptDeleteTitle: "Do you really want to delete this user?",
    promptDeleteContent1: "You are about to delete the user",
    promptDeleteContent2:
      "He will no longer be able to connect and will lose all his work in progress. Do you agree?",
    promptRevokeAdminTitle:
      "Do you want to revoke administrative rights for this user?",
    promptRevokeAdminContent1:
      "You are about to revoke the administrative rights of the user",
    promptRevokeAdminContent2:
      "He will no longer be able to connect to this administration interface. Do you agree?",
    promptGrantAdminTitle: "Give administrative rights to this user?",
    promptGrantAdminContent1:
      "You are about to give administrative rights to the user",
    promptGrantAdminContent2: "Do you agree?",
    firstName: "First name",
    lastName: "Name",
    email: "Email",
    emailConfirmed: "Email confirmed",
    role: "Role",
    confirmedDeletion: "The user has been deleted.",
    confirmedToggleAdminRights: "The user's rights have been updated."
  },
  groups: {
    title: "Groups",
    create: "Create a group",
    edit: "Edit",
    delete: "Delete",
    promptDeleteGroupTitle: "Do you really want to delete this group?",
    promptDeleteGroupContent1: "You are about to delete the",
    promptDeleteGroupContent2:
      "All users attached to the latter will lose the rights they had with it. Do you agree?",
    columnTitle: "Group name",
    created: "The group has been created.",
    updated: "The group has been updated.",
    deleted: "The group has been deleted."
  },
  models: {
    title: "Models",
    create: "Create a model",
    edit: "Edit",
    delete: "Delete",
    promptDeleteModelTitle: "Do you really want to delete this model?",
    promptDeleteModelContentMultiple:
      "A multiple model cannot be deleted as long as it is associated with a page and has created objects.",
    promptDeleteModelContentSimple:
      "If the model is simple, it will be deleted with its object.",
    columnTitle: "Model name",
    columnCategory: "Category",
    columnType: "Type",
    confirmedDeletion: "The model has been deleted."
  },
  settings: {
    siteSettings: "Website parameters",
    siteName: "Site name",
    siteUrl: "Site address",
    mainLanguage: "Main language",
    secondaryLanguages: "Secondary languages",
    interfaceColor: "Interface main color",
    interfaceLogo: "Interface logo",
    ftpSettings: "FTP settings",
    ftpHost: "Host",
    ftpUser: "User",
    ftpPort: "Port",
    password: "FTP password",
    setPassword: "Configure the ftp password",
    updated: "Settings updated.",
    updatedPassword: "Password updated."
  },
  pagesEdit: {
    title: "Title",
    model: "Model",
    modelHint:
      "By associating a multiple model with a page, as many pages as there are objects linked to it will be generated. Only multiple models not already linked to a page are available.",
    description: "Description",
    template: "Template",
    homepage: "Home page",
    homepageHint:
      "If another page was the home page, it will no longer be, in favour of this one.",
    pageUpdated: "Page updated.",
    pageCreated: "Page created."
  },
  usersEdit: {
    firstName: "First name",
    lastName: "Last name",
    email: "Email",
    groups: "Groups",
    userCreated: "The user has been created.",
    userUpdated: "The user has been updated."
  },
  groupsEdit: {
    name: "Group name",
    created: "The group has been created.",
    updated: "The group has been updated."
  },
  modelitems: {
    create: "Create an item",
    edit: "Edit",
    delete: "Delete",
    promptDeleteTitle: "Do you really want to delete this item?",
    columnTitle: "Name",
    columnStatus: "Status",
    columnCreated: "Creation date",
    columnModified: "Last modification",
    deleted: "The item has been deleted.",
    search: "Search"
  },
  modelItemsEdit: {
    title: "Item fields",
    settings: "Item settings",
    groupsHint:
      "Allows assigning ownership of objects to one or more user groups, controlling access and permissions to those objects based on group membership.",
    statusHint:
      "Status of the object. Can affect its visibility for non-administrators.",
    statuses: {
      active: "Active",
      archived: "Archived",
      draft: "Draft",
      inactive: "Inactive",
      pending: "Pending"
    },
    error: "An error occurred when editing this object.",
    updated: "Item has been updated.",
    created: "Item has been created.",
    creator: "Creator"
  },
  modelsEdit: {
    name: "Model name",
    type: "Model type",
    typeHint:
      "A single model will have a single associated object (e.g. contact information), whereas a multiple model can have several (e.g. blog posts).",
    category: "Model category",
    categoryHint:
      "Organise models in categories, particularly in the models menu.",
    addField: "Add a field",
    modelFieldsManagement: "Model field management",
    fieldName: "Field name",
    fieldType: "Field type",
    fieldChoice: "Choice",
    fieldModel: "Model",
    fieldRequired: "Required",
    promptCategoryList: "Category list",
    categoryName: "Category name",
    fieldName: "Name",
    fieldDescription: "Description",
    modelTypeSimple: "Simple",
    modelTypeMultiple: "Multiple",
    fieldsType: {
      text: "Text",
      number: "Number",
      choice: "Choice",
      multipleChoice: "Multiple choice",
      manytoone: "Many-to-one relationship",
      onetomany: "One-to-many relationship",
      color: "Color",
      image: "Image",
      file: "File",
      editor: "WYSIWYG Editor"
    }
  },
  navigation: {
    noModel: "No data model to display.",
    data: "Data",
    administration: "Administration",
    models: "Data models",
    users: "Users",
    groups: "Groups",
    permissions: "Permissions",
    logs: "Logs",
    settings: "Settings",
    staticWebsite: "Static website",
    pages: "Pages",
    theme: "Theme",
    logout: "Log out"
  },
  routes: {
    pagesList: "Pages list",
    pagesCreate: "Create a page",
    pagesEdit: "Edit a page",
    usersList: "Users list",
    usersCreate: "Create a user",
    usersEdit: "Edit a user",
    groupsList: "Groups list",
    groupsCreate: "Create a group",
    groupsEdit: "Edit a group",
    permissionsList: "Permissions list",
    permissionsEdit: "Edit a permission",
    modelsList: "Models list",
    modelsCreate: "Create a model",
    modelsEdit: "Edit a model",
    modelItemsList: "Items list",
    modelItemsCreate: "Create an item",
    modelItemsEdit: "Edit an item",
    importTheme: "Import a theme",
    settings: "Settings",
    logs: "Logs"
  },
  permissions: {
    title: "Permissions",
    permissionsFor: "Permissions for",
    canAdd: "Who can add",
    canEdit: "Who can edit",
    canView: "Who can view",
    canDelete: "Who can delete",
    publicModelDefinition: "Publicly accessible model definition",
    isCreatorVisible: "Visible creator details",
    everyone: "Everyone",
    creator: "Creator",
    someGroups: "Some groups",
    administrators: "Administrators",
    visible: "Visible",
    notVisible: "Not visible"
  },
  permissionsEdit: {
    info:
      "To set up permissions for viewing, editing, adding, and deleting objects from the model, you can use three parameters: Groups, Enable Groups Ownership, and Allow Everyone.",
    info2:
      "By carefully selecting the appropriate parameters for each action, you can ensure that only authorized users have the necessary permissions to perform the desired actions on your objects.",
    info3:
      "For each action, select the appropriate parameter to control who has permission to perform that action.",
    infoEnableGroups:
      "To enable groups to perform the action, select the Groups parameter and attach the relevant groups.",
    infoEnableGroupsOwnership:
      "Alternatively, you can choose to allow groups that have ownership over an object to perform the action by selecting Enable Groups Ownership.",
    infoAllowEveryone:
      "<strong>Use caution when selecting Allow Everyone</strong>, as this will grant permission to all users and <span class='text-negative text-bold'>could compromise the security of your system</span>.",
    groups: "Groups",
    groupsOwnObject: "Enable groups to own their objects",
    allowEveryone: "Allow everyone",
    allowCreator: "Allow creator",
    misc: "Miscellaneous"
  },
  buttonGenerator: {
    publish: "Publish",
    published: "The website and its changes are now online!"
  },
  extrasTable: {
    title: "Free variable for the field",
    addVar: "Add a variable",
    key: "Key",
    value: "Value"
  },
  fileInput: {
    send: "Send a file",
    sendHint: "Maximum size of 2 megabytes",
    sendError: "This file is over 2 megabytes in size"
  },
  imageInput: {
    send: "Send an image",
    sendHint: "Maximum size of 2 megabytes",
    sendError: "This file is over 2 megabytes in size",
    delete: "Remove image"
  }
};
