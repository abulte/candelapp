// Translations are stored by components / pages

export default {
  misc: {
    others: "Autres",
    required: "Requis",
    example: "Exemple",
    minimumCharacters: "caractères minimum",
    maximumCharacters: "caractères maximum"
  },
  welcome: {
    welcome: "Bienvenue"
  },
  login: {
    email: "Email",
    password: "Password",
    login: "Log in",
    notAuthorized: "You are not authorised to log in here."
  },
  404: {
    message: "Oh, il n'y a rien à voir par ici...",
    return: "Retourner à la page de modélisation"
  },
  logs: {
    title: "Journaux",
    entry: "Entrée",
    date: "Date"
  },
  pages: {
    title: "Pages",
    create: "Créer une page",
    edit: "Éditer",
    delete: "Supprimer",
    homepage: "Page d'accueil",
    promptDeleteTitle: "Voulez-vous vraiment supprimer cette page ?",
    promptDeleteMessageNoModel:
      "Vous êtes sur le point de supprimer cette page. Cette page dans toutes ses langues ne sera plus disponible lors de la prochaine mise à jour du site. Êtes-vous d'accord ?",
    promptDeleteMessageWithModel:
      "Vous êtes sur le point de supprimer cette page dans toutes ses langues. Cette page est aussi liée à un modèle de données. Tous les objets de ce modèle n'auront plus leur page de générée. Êtes-vous d'accord ? ",
    entryTitle: "Page",
    entryDataModel: "Modèle de données associé",
    deleteConfirm: "La page a bien été supprimée."
  },
  theme: {
    send: "Envoyer",
    lastUpdate: "Dernière mise à jour le",
    templateList: "Liste des gabarits",
    importTheme: "Importer un thème",
    fileSizeHint: "Fichier .zip inférieur à 10 megaoctets",
    fileSizeError: "Le fichier dépasse la taille autorisée",
    confirmationImported: "Le thème a bien été importé."
  },
  users: {
    title: "Utilisateurs",
    create: "Créer un utilisateur",
    admin: "Administrateur",
    user: "Utilisateur",
    producer: "Producteur",
    reuser: "Réutilisateur",
    edit: "Éditer",
    delete: "Supprimer",
    revokeAdmin: "Révoquer les droits administrateur",
    grantAdmin: "Donner les droits administrateur",
    promptDeleteTitle: "Voulez-vous vraiment supprimer cet utilisateur ?",
    promptDeleteContent1: "Vous êtes sur le point de supprimer l'utilisateur",
    promptDeleteContent2:
      "Il ne pourra plus se connecter et perdra tous ses travaux en cours. Êtes-vous d'accord ?",
    promptRevokeAdminTitle:
      "Voulez-vous révoquer les droits d'administration à cet utilisateur ?",
    promptRevokeAdminContent1:
      "Vous êtes sur le point de révoquer les droits d'administration à l'utilisateur",
    promptRevokeAdminContent2:
      "Il ne pourra plus se connecter à cette interface d'administration. Êtes-vous d'accord ?",
    promptGrantAdminTitle:
      "Donner les droits d'administration à cet utilisateur ?",
    promptGrantAdminContent1:
      "Vous êtes sur le point de donner des droits d'administration à l'utilisateur",
    promptGrantAdminContent2: "Êtes-vous d'accord ?",
    firstName: "Prénom",
    lastName: "Nom",
    email: "Email",
    emailConfirmed: "Email confirmé",
    role: "Rôle",
    confirmedDeletion: "L'utilisateur a bien été supprimé.",
    confirmedToggleAdminRights:
      "Les droits de l'utilisateur ont bien été mis à jour."
  },
  groups: {
    title: "Groupes",
    create: "Créer un groupe",
    edit: "Éditer",
    delete: "Supprimer",
    promptDeleteGroupTitle: "Voulez-vous vraiment supprimer ce groupe ?",
    promptDeleteGroupContent1: "Vous êtes sur le point de supprimer le groupe",
    promptDeleteGroupContent2:
      "Tous les utilisateurs rattachés à ce dernier perdront les droits qu'ils avaient avec. Êtes-vous d'accord ?",
    columnTitle: "Nom du groupe",
    created: "Le groupe a bien été créé.",
    updated: "Le groupe a bien été mis à jour.",
    deleted: "Le groupe a bien été supprimé."
  },
  models: {
    title: "Modèles",
    create: "Créer un modèle",
    edit: "Éditer",
    delete: "Supprimer",
    promptDeleteModelTitle: "Voulez-vous vraiment supprimer ce modèle ?",
    promptDeleteModelContentMultiple:
      "Un modèle multiple ne peut être supprimé tant qu'il est associé à une page et possède des objets créés.",
    promptDeleteModelContentSimple:
      "Si le modèle est simple, il sera supprimé avec son objet.",
    columnTitle: "Nom du modèle",
    columnCategory: "Catégorie",
    columnType: "Type",
    confirmedDeletion: "Le modèle a bien été supprimé."
  },
  settings: {
    siteSettings: "Paramètres du site",
    siteName: "Nom du site",
    siteUrl: "Adresse du site",
    mainLanguage: "Langue principale",
    secondaryLanguages: "Langues secondaires",
    interfaceColor: "Couleur de l'interface",
    interfaceLogo: "Logo de l'interface",
    ftpSettings: "Paramètres FTP",
    ftpHost: "Hôte",
    ftpUser: "Utilisateur",
    ftpPort: "Port",
    password: "Mot de passe FTP",
    setPassword: "Configurer le mot de passe FTP",
    updated: "Paramètres mis à jour",
    updatedPassword: "Mot de passe mis à jour."
  },
  pagesEdit: {
    title: "Titre",
    model: "Modèle",
    modelHint:
      "En associant un modèle multiple à une page, autant de pages qu'il y a d'objets liés à ce dernier seront générées. Seuls les modèles multiples et non déjà liés à une page sont disponibles",
    description: "Description",
    template: "Gabarit",
    homepage: "Page d'accueil",
    homepageHint:
      "Si une autre page était la page d'accueil, cette dernière ne le sera plus au profit de celle-çi.",
    pageUpdated: "Page mise à jour.",
    pageCreated: "Page créée."
  },
  usersEdit: {
    firstName: "Prénom",
    lastName: "Nom",
    email: "Email",
    groups: "Groupes",
    userCreated: "L'utilisateur a bien été créé.",
    userUpdated: "L'utilisateur a bien été mis à jour."
  },
  groupsEdit: {
    name: "Nom du groupe",
    created: "Le groupe a bien été créé.",
    updated: "Le groupe a bien été mis à jour."
  },
  modelitems: {
    create: "Créer un objet",
    edit: "Éditer",
    delete: "Supprimer",
    promptDeleteTitle: "Voulez-vous vraiment supprimer cet objet ?",
    columnTitle: "Titre",
    columnStatus: "Statut",
    columnCreated: "Date de création",
    columnModified: "Dernière modification",
    deleted: "L'objet a bien été supprimé.",
    search: "Rechercher"
  },
  modelItemsEdit: {
    title: "Champs de mon objet",
    settings: "Paramètres de l'objet",
    groupsHint:
      "Permet d'attribuer la propriété d'objets à un ou plusieurs groupes d'utilisateurs et de contrôler l'accès et les autorisations à ces objets en fonction de l'appartenance au groupe.",
    statusHint:
      "Statut de l'objet. Peut affecter sa visibilité pour les non-administrateurs.",
    statuses: {
      active: "Actif",
      archived: "Archivé",
      draft: "Brouillon",
      inactive: "Inactif",
      pending: "En attente"
    },
    error: "Une erreur est survenue à l'édition de cet objet.",
    updated: "Objet mis à jour.",
    created: "Objet créé.",
    creator: "Créateur"
  },
  modelsEdit: {
    name: "Nom du modèle",
    type: "Type de modèle",
    typeHint:
      "Un modèle simple aura un seul objet associé (Ex: Informations de contact) quand un modèle multiple va pouvoir en avoir plusieurs (Ex: Articles de blog).",
    category: "Catégorie de modèle",
    categoryHint:
      "Permettre d'organiser des modèles dans des catégories, notamment dans le menu des modèles.",
    addField: "Ajouter un champ",
    modelFieldsManagement: "Gestion des champs du modèle",
    fieldName: "Nom du champ",
    fieldType: "Type du champ",
    fieldChoice: "Choix",
    fieldModel: "Modèle",
    fieldRequired: "Requis",
    promptCategoryList: "Liste des catégories",
    categoryName: "Nom de la catégorie",
    fieldName: "Titre de l'objet",
    fieldDescription: "Description de l'objet",
    modelTypeSimple: "Simple",
    modelTypeMultiple: "Multiple",
    fieldsType: {
      text: "Texte",
      number: "Nombre",
      choice: "Choix",
      multipleChoice: "Choix multiple",
      manytoone: "Relation plusieurs-à-un",
      onetomany: "Relation un-à-plusieurs",
      color: "Couleur",
      image: "Image",
      file: "Fichier",
      editor: "Éditeur WYSIWYG"
    }
  },
  navigation: {
    noModel: "Aucun modèle à afficher.",
    data: "Données",
    administration: "Administration",
    models: "Modèles de données",
    users: "Utilisateurs",
    groups: "Groupes",
    permissions: "Permissions",
    logs: "Journaux",
    settings: "Paramètres",
    staticWebsite: "Site statique",
    pages: "Pages",
    theme: "Thème",
    logout: "Se déconnecter"
  },
  routes: {
    pagesList: "Liste des pages",
    pagesCreate: "Créer une page",
    pagesEdit: "Éditer une page",
    usersList: "Liste des utilisateurs",
    usersCreate: "Créer un utilisateur",
    usersEdit: "Éditer un utilisateur",
    groupsList: "Liste des groupes",
    groupsCreate: "Créer un groupe",
    groupsEdit: "Éditer un groupe",
    permissionsList: "Liste des permissions",
    permissionsEdit: "Éditer une permission",
    modelsList: "Liste des modèles",
    modelsCreate: "Créer un modèle",
    modelsEdit: "Éditer un modèle",
    modelItemsList: "Liste des objets",
    modelItemsCreate: "Créer un objet",
    modelItemsEdit: "Éditer un objet",
    importTheme: "Importer un thème",
    settings: "Paramètres",
    logs: "Journaux"
  },
  permissions: {
    title: "Permissions",
    permissionsFor: "Permissions pour",
    canAdd: "Qui peut ajouter",
    canEdit: "Qui peut éditer",
    canView: "Qui peut voir",
    canDelete: "Qui peut supprimer",
    publicModelDefinition: "Définition du modèle publiquement accessible",
    isCreatorVisible: "Détails du créateur visible",
    everyone: "Tout le monde",
    creator: "Créateur",
    someGroups: "Certains groupes",
    administrators: "Administrateurs",
    visible: "Visible",
    notVisible: "Non visible"
  },
  permissionsEdit: {
    info:
      "Pour définir les autorisations de visualisation, d'édition, d'ajout et de suppression d'objets dans le modèle, vous pouvez utiliser trois paramètres : 'Groupes', 'Activer la propriété de groupe' et 'Autoriser tout le monde'.",
    info2:
      "En sélectionnant soigneusement les paramètres appropriés pour chaque action, vous pouvez vous assurer que seuls les utilisateurs autorisés disposent des permissions nécessaires pour effectuer les actions souhaitées sur vos objets.",
    info3:
      "Pour chaque action, sélectionnez le paramètre approprié afin de déterminer qui a le droit d'effectuer cette action.",
    infoEnableGroups:
      "Pour permettre aux groupes d'effectuer l'action, sélectionnez le paramètre Groupes et attachez les groupes concernés.",
    infoEnableGroupsOwnership:
      "Vous pouvez également choisir d'autoriser les groupes qui ont la propriété d'un objet à effectuer l'action en sélectionnant Activer la propriété des groupes.",
    infoAllowEveryone:
      "<strong>Soyez prudent lorsque vous sélectionnez Autoriser tout le monde</strong>, car l'autorisation est accordée à tous les utilisateurs, <span class='text-negative text-bold'>ce qui pourrait compromettre la sécurité de votre système</span>.",
    groups: "Groups",
    groupsOwnObject: "Permettre aux groupes de s'approprier leurs objets",
    allowEveryone: "Autoriser tout le monde",
    allowCreator: "Autoriser le createur",
    misc: "Divers"
  },
  buttonGenerator: {
    publish: "Publier",
    published: "Le site et ses modifications sont en ligne !"
  },
  extrasTable: {
    title: "Variables libres pour le champ",
    addVar: "Ajouter une variable",
    key: "Clé",
    value: "Valeur"
  },
  fileInput: {
    send: "Envoyer un fichier",
    sendHint: "Taille maximum de 2 megaoctets",
    sendError: "La taille de ce fichier dépasse les 2 megaoctets"
  },
  imageInput: {
    send: "Envoyer une image",
    sendHint: "Taille maximum de 2 megaoctets",
    sendError: "La taille de ce fichier dépasse les 2 megaoctets",
    delete: "Supprimer l'image"
  }
};
