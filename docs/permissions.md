# Permissions

Candelapp provides a comprehensive permission system, offering fine-grained control over who can view, edit, add, or delete objects from each model. Permissions can be controlled using three parameters: Groups, Enable Group Ownership, and Allow Everyone. Here's how you can use each of them:

## Groups
This parameter is used to grant specific groups permission to perform actions on the model's objects. Simply select 'Groups' and assign the relevant groups that should have the permissions.

## Enable group ownership
This parameter is a bit more specific - it allows groups that 'own' an object to perform actions on that object. If the group is designated as the owner of a particular object, members can perform the specified actions.

## Allow Everyone
This parameter should be used with caution. Selecting this grants all users the ability to perform the action on the objects. While it may be useful in certain scenarios, it can compromise system security if not used judiciously.

By judiciously managing these parameters for each action, you can ensure that your data remains secure, and only authorized users or groups can perform actions on your models.
