# How does it work

## Tenant separation

Candelapp utilizes the [django-tenant](https://github.com/django-tenants/django-tenants) library, which is designed to work with PostgreSQL's built-in schema functionality. This creates a separate namespace for each tenant's data, effectively isolating each tenant's data from others. Candelapp identifies tenants based on the domain in each request. This means that when a request is made, the system checks the domain of the request to determine which tenant's schema should be used to process the request. This enables the system to effectively manage multi-tenancy in a secure and isolated manner.

## Data management

It uses a hybrid approach that combines features of both relational and non-relational databases. It leverages PostgreSQL's JSONField, which allows you to store JSON data in your tables. This provides the flexibility and scalability of a NoSQL database, letting you handle a wide variety of data types and structures, while still maintaining some of the benefits of a structured, relational database. This approach enables Candelapp to handle complex, diverse datasets.

Candelapp's core functionality is centered around two internal models - CModel and CModelItem.

### CModel
CModel is where the definitions of all your data models are stored. This meta-model allows you to define the structure and data type of your data models, essentially serving as a blueprint for the data you want to store. It provides a variety of field types that you can use to customize your data models, making it highly flexible and adaptable to your specific data needs.

```
# Here is an example of the fields for a CModel called 'blogposts'.
[
 {'type': 'text',
  'extras': [],
  'required': True,
  'verbose_name': 'Name',
  'internal_name': 'title'},
 {'type': 'text',
  'extras': [],
  'required': False,
  'verbose_name': 'Description',
  'internal_name': 'description'},
 {'type': 'wysiwyg',
  'extras': [{}],
  'required': True,
  'verbose_name': 'Content',
  'internal_name': 'content'},
 {'type': 'onetomany',
  'model': 2,
  'extras': [{}],
  'required': True,
  'verbose_name': 'Author',
  'internal_name': 'author'}
]
```

### CModelItem
CModelItem, on the other hand, is the model that actually stores your data, reflecting the structure and data types defined in the associated CModel. When you create or update a CModelItem, it's like creating or updating an instance of the data model defined in CModel.

```
# Example of the data from an instance of "blogposts"
{'title': 'A blogpost title',
 'author': 1,
 'content': '<div><b>Lorem ipsum</b><br></div><div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>',
 'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'}
```

Under the hood, when a CModelItem is created or updated, a serializer is used to validate all the fields in the payload against the model definition stored in the corresponding CModel. This ensures that the data being saved into the CModelItem matches the structure and data types defined in CModel, providing a validation mechanism and ensuring data integrity. This approach makes Candelapp a tool for handling complex data structures.

⚠ It's important to note that if you update the CModel definition, there isn't an automatic check or update process for existing CModelItems tied to that CModel. This approach has been taken to ensure flexibility in data management.

However, this means that if you want to keep your CModelItems consistent with the updated CModel definition, you'll need to perform data migrations yourself to update the existing CModelItems.

This approach may change in the future, with the potential implementation of an automated system to maintain strict data integrity across all CModelItems when a CModel is updated. This would further enhance the consistency and reliability of data within Candelapp.
