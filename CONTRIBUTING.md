# Contributing

## Install the project locally with docker

Install docker and [docker-compose](https://docs.docker.com/compose/install/).

```bash
docker-compose up
```

To get into shell_plus (set `public` for public schemas or `fsociety` or `sacredheart`):

```bash
docker exec -t api_candelapp tenant_command shell_plus -s (public|fsociety|sacredheart)
```

If needed, you can enter inside backend container:

```bash
docker exec -ti api_candelapp bash
```

Run migrations:

```bash
pdm run python3 manage.py migrate_schemas
```

Different endpoints will be availabe at the following addresses (when using the tenant "fsociety"):
- Backoffice http://fsociety.localhost
- API http://api.fsociety.localhost/api/v1/schema/
- API Documentation http://api.sacredheart.localhost/api/v1/schema/


By default, two users are already set on each tenants:

- admin@candelapp.com / candelapp
- user@candelapp.com / candelapp

## Locally without docker

Prerequisite: a running PostgreSQL server.

Launch API:

```bash
pdm run python3 manage.py runserver
```

Launch frontend:

```bash
cd front
yarn run dev
```

NB: `yarn run dev` uses `NODE_OPTIONS=--openssl-legacy-provider` for compatibility with modern Node versions (at least until Quasar is upgraded).

## Run tests

```bash
DJANGO_CONFIGURATION=Test pdm run pytest
```

## Misc

Useful command aliases

```bash
#Fast tests with xdist
alias dtestf="time docker exec -t api_candelapp env DJANGO_CONFIGURATION=Test pdm run pytest -n auto --dist loadfile"

#Slow tests with output
alias dtests="time docker exec -t api_candelapp env DJANGO_CONFIGURATION=Test pdm run pytest -s"

# Open a shell with a tenant (Ex: dshell fsociety)
alias dshell="docker exec -ti api_candelapp pdm run python3 manage.py tenant_command shell_plus -s"

# Open a jupyter notebook with a tenant (Ex: dnotebook fsociety)
alias dnotebook="docker exec -ti api_candelapp pdm run python3 manage.py tenant_command shell_plus --notebook -s"

# Open the api container
alias dcontainer="docker exec -ti api_candelapp bash"
```

Don't forget to reload your rc
`source ~/.zshrc` or with the configuration file of the shell you are using.

## Maintenance

### Dump data from live environments to dev one

On one live server, dump data with:
`docker exec -ti api_candelapp pdm run python3 manage.py tenant_command dumpdata -s <my_tenant> > dump.json`

On the dev environment, load data with:

```bash
docker cp fixtures.json api_candelapp:/home/candelapp/fixtures.json
docker exec -ti api_candelapp bash
pdm run python3 manage.py tenant_command loaddata -s <my_tenant> fixtures.json
```

## Translations

To fetch new strings to translate in french:

```bash
pdm run python3 manage.py makemessages -l fr --ignore="__pypackages__" --ignore="front"
```

You can then translate these strings using a software such as Poedit.

If you are not pushing the modifications to the official repository, don't forget to compile the messages with:

```bash
pdm run python3 manage.py compilemessages
```
