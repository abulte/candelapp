FROM python:3.10-slim-buster

RUN apt-get -qq update && \
    apt-get install -y postgresql-client libpq-dev lftp vim curl gettext && \
    apt-get clean

ENV HOME=/home/candelapp

RUN groupadd -r candelapp -g 1000 && useradd -u 1000 -r -g candelapp -m -d $HOME -s /sbin/nologin -c "Candelapp user" candelapp && chmod 755 $HOME

ARG UID=1000
ARG GID=1000
USER candelapp

WORKDIR $HOME

COPY --chown=1000:1000 . $HOME/

# Is only useful when running the container with Dev env. Use django statics instead of S3
RUN mkdir -p $HOME/media
RUN chown 1000:1000 $HOME/media

# Install pdm, add to path, install packages
RUN python3 -m pip install --upgrade pip

# PDM bug, install it with requests below 2.30.0 for now
# Can be removed when fixed
RUN python3 -m pip install pdm

ENV PATH $HOME/.local/bin:$PATH
RUN pdm self update
RUN pdm sync --clean

# Compile messages for translations
RUN pdm run python3 manage.py compilemessages --ignore .venv
