export ALLOWED_HOSTS='["api.backoffice.localhost"]'
export CORS_ALLOWED_ORIGINS='["http://backoffice.localhost:4000", "http://api.localhost:3000"]'
export POSTGRES_HOST=localhost
export POSTGRES_DB=candelapp
export POSTGRES_USER=postgres
export POSTGRES_PASSWORD=postgres
export DJANGO_SETTINGS_MODULE=instance.settings
export DJANGO_CONFIGURATION=Dev
export SECRET_KEY=candelapp

pdm run gunicorn instance.wsgi:application --timeout 60 --workers 2 --reload --bind 0.0.0.0:8000